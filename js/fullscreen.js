
function errorHandler() {
   alert('mozfullscreenerror');
}
document.documentElement.addEventListener('mozfullscreenerror', errorHandler, false);

var toggleFlag = 0;


function toggleFullScreen() {
	
	if(toggleFlag) {
		$("#caWrapper").css({"height":"75%", "width":"75%"});
		$("#fsIcon").attr('src','images/fullscreen.png');
		toggleFlag = 0;
	} else {
		$("#caWrapper").css({"height":"100%", "width":"100%"});
		$("#fsIcon").attr('src','images/resize.png');
		toggleFlag = 1;
	}
	
  if (!document.fullscreenElement &&    
      !document.mozFullScreenElement && !document.webkitFullscreenElement) { 
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}


document.addEventListener('keydown', function(e) { 
  if (e.keyCode == 27) { 
    toggleFullScreen();
  }
}, false);