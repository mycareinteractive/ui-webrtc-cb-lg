$(document).ready(function() {	
		app.init();
	});
	
	//var baseURL = window.location.href.replace(/\/[^\/]+$/, "/"),
	var baseURL = "https://192.168.77.57:8443/Martti_Cleveland/",
		meetingURL = '',
		assignedVMR = '',
		intervalRef;
	var app = {
		init : function(){
			$("#loader").show();
			app.fetchVMR();	
			$(".inviteMenu ul li").click(app.menuClick.bind(this));
			$(".sendInvite").click(app.sendInvite.bind(this));
			$(".decline").click(app.declineCall);
			$(".join").click(app.joinCall);
			
		},		
		/* Dynamically create your VMR */
		fetchVMR : function() {
			$.ajax({
				type : 'GET',
				dataType : "json",
				url : baseURL+"rest/CNX/getWebUrl/Spanish",
				cache : false,
				success : function(data) {
					meetingURL = data.cnxURL;
					var extractVMR = data.cnxURL.split('/');
					assignedVMR = extractVMR[extractVMR.length - 1];
					$("#loader").hide();
					$(".assingedVMR").html(assignedVMR);
					$("#room_no").val(assignedVMR);
					
					intervalRef = setInterval(app.pollPariticipant, 10000);
				},
				error: function(e) {
				    $("#loader").hide();
					alert("Conference creation failed");					
		        }
				
			});
		},
		/* Send Invite to recipient via Email, Phone */
		sendInvite : function(ele) {
			var mobile='', email='', videoConfURL='';
			if(ele.target.attributes[0].nodeValue=="phone") {
				mobile = $("#mobile").val();
				//videoConfURL = "martti://ClevelandClinic?dialString="+assignedVMR+"@join.carenection.com";
				videoConfURL = assignedVMR;
				if(mobile=="") {
					alert("Enter Invitee Phone Number");
					return false;
				}
			}
			if(ele.target.attributes[0].nodeValue=="email") {
				email = $("#email").val();
				videoConfURL = meetingURL;
				if(email=="") {
					alert("Enter Invitee Email Address");
					return false;
				}
			}						
			$("#loader").show();
			var postData =  {
								"domain": "",
								"username": "",
								"password": "",
								"email": email,
								"phoneNumber": mobile,
								"language": "Spanish",
								"meetingURL": videoConfURL
							}
			$.ajax({
				method  : 'POST',
				url     : baseURL+"rest/CNX/sendinvite",
				data    : JSON.stringify(postData), 
				headers : {'Content-Type': 'application/json'},
				success : function(data) {
					$("#loader").hide();					
					alert("Invitation sent successfully.");	
				},
				error: function(e) {
					$("#loader").hide();
					alert("Send Invite failed. Please try again with valid information.");					
		        }				
			});
		},	
		/* Poll for participant joining your VMR*/
		pollPariticipant : function() {
			
			$.ajax({
				method  : 'GET',
				url     : baseURL+"rest/CNX/conferences/"+assignedVMR+"/participants",
				headers : {'Content-Type': 'application/json'},
				success : function(data) {
					console.log("New participants found.");						
					if(data["cnx-participants-count"] > 0) {
						$("#inviteName").html(" by "+ data["cnx-participants"]["participant-name"]);
						$("#callAlert").slideDown();
					}
					
				},
				error: function(e) {					
					console.log("No participants found.");					
		        }				
			});
		},
		/* Decline the call */
		declineCall : function () {
			$("#callAlert").slideUp();
		},
		/* Join the call */
		joinCall : function () {
			clearInterval(intervalRef);
			connect();
		},
		menuClick: function(ele) {
			$(".inviteMenu ul li").removeClass("inviteMenuSelected")
			$(ele.target).addClass("inviteMenuSelected");
			$(".inviteContent>div").hide()
			$("#"+ele.target.attributes[0].nodeValue).show();
		}
		
	};