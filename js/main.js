/*global $ , SocketHandler, video_src, window, count, localStream, NumberOfUsers*/
/* globals SocketHandler , count:true  , video_src:true, localStream:true  */
/* exported  changeDisplay , gotRemoteStream , showChat, insertCaptionLocal , userLeft ,
changeStyle , askConfirmation , RedirectToLogin , cleanAll , ReturnToMeeting , muteUnmuteLocal ,
pauseLocal , hideSelfView ,mySocketHandler*/
"use strict";
var mySocketHandler = new SocketHandler("https://meetme.carenection.com");
/*document.getElementById('start').addEventListener('click', function () {
    if(validateForm()){
    var username = $("#username").val();
    var email = $("#email_id").val();
    var roomId = $("#room_no").val();
    $(document).trigger("loginEvent", [username, email, roomId]); }
});
document.getElementById('connect1').addEventListener('click', function () {
     $(document).trigger("connectEvent");
});
document.getElementById('disconnect').addEventListener('click', function () {
     askConfirmation();
});
document.getElementById('return').addEventListener('click', function () {
     ReturnToMeeting();
});
document.getElementById('Confirmdisconnect').addEventListener('click', function () {
     $(document).trigger("confirmDisconnectEvent");
});
document.getElementById('logout').addEventListener('click', function () {
     $(document).trigger("logoutEvent");
});
document.getElementById('chat').addEventListener('click', function () {
     showChat(true);
});
document.getElementById('chatbox').addEventListener('keypress', function () {
      $(document).trigger("SendMsgEvent");
});
/*
$(document).ready(function () {
        var username = $("#username").val();
        var email = $("#email_id").val();
        var roomId = $("#room_no").val();
        var server = $("#server").val();
        $(document).trigger("loginEvent", [username, email, roomId]);
        
});*/

$(document).on("getMessageTextEvent", function () {
    var msg = $("#chatbox").val();
    var textnode = document.createTextNode("Me" + ": " + $("#chatbox").val());
    $("#msg-div").append(textnode);
    var br = document.createElement("br");
    $("#msg-div").append(br);
    $(document).trigger("getMessageTextEventResponse", msg);
    //$("#chatbox").value = "";
    $("#chatbox").val("");
});

$(document).on("gotTextMessageEvent", function (event, response) {
    var textnode = document.createTextNode(response.UserDisplayName + ": " + response.Content);
    $("#msg-div").append(textnode);
    var br = document.createElement("br");
    $("#msg-div").append(br);
});
/*
function validateForm() {
   console.log("hello validated");
    if($("#username").val() === "") {
       alert("enter your name.");
        return;
    } else if($("#email_id").val() === "") {
        alert("enter the email id.");
        return;
    } else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email_id").val()))) {
    alert("enter the proper email id.");
        return;
    } else if($("#room_no").val() === "") {
         alert("enter the room no.");
        return;
    }
    return true;
}
function loginfun() {
     var username = $("#username").val();
    var email = $("#email_id").val();
    var roomId = $("#room_no").val();
    $(document).trigger("loginEvent", [username, email, roomId]);
}
$(document).ready(function(){
    $("form[name='formName']").validate({
        rules: {
            Name: {
                required: true,
                text: true
            },
            email: {
                required: true,
                email:true
            },
            room: {
                required: true,
                //isNumber: true
            }
        },
         messages: {
      Name: "Please enter your firstname",
      email: "Please enter a valid email address",
     room: "Please enter room number"
    },
       submitHandler: function(form) {
         alert("submitted.");
      form.submit();
    }
    });
})
*/
$(document).on("changeStyleEvent", function () {
    $("#first").addClass("none");
    $("#second").addClass("show");
});

$(document).on("getUserMediaSuccessEvent", function (event, localStreamSrc) {
    var localVideo = document.createElement('video');
    localVideo.autoplay = true;
    localVideo.id = 'localVideo';
    localVideo.className = 'localVideo';
    localVideo.src = localStreamSrc;
    var t = document.getElementById("videos");
    t.appendChild(localVideo);
});

$(document).on("changeDisplayEvent", function () {
    if (NumberOfUsers === 1) {
        notify("", 3);
    } else if (NumberOfUsers > 1) {
        notify("", 4);
    }
    var l = document.getElementById("localVideo_bottom");
    $("#command_join").addClass("none");
    $("#local_v").addClass("show");
    $("#options").addClass("show");
    var i;
    for (i = 0; i < video_src.length; i++) {
        if (video_src[i].name === "local") {
            l.src = video_src[i].src;
            break;
        }
    }
    $("#localVideo").remove();
});

$(document).on("gotRemoteStreamEvent", function (thisEvent, event, response) {
    var t = document.getElementById("videos");
    var remoteVideo = document.createElement("video");
    remoteVideo.className = "try";
    remoteVideo.autoplay = true;
    remoteVideo.src = window.URL.createObjectURL(event.stream);
    var new_div = document.createElement('div');
    if (count === 1) {
        new_div.id = "new_div" + count;
        new_div.className = "remote-video1";
    }
    if (count === 2) {
        document.getElementById("new_div1").className = "third-row1-1";
        new_div.id = "new_div" + count;
        new_div.className = "third-row1-2";
    }
    if (count === 3) {
        document.getElementById("new_div1").className = "fourth-row1-1";
        document.getElementById("new_div2").className = "fourth-row1-2";
        new_div.id = "new_div" + count;
        new_div.className = "fourth-row2-1";
    }
    if (count === 4) {
        document.getElementById("new_div1").className = "fourth-row1-1";
        document.getElementById("new_div2").className = "fourth-row1-2";
        document.getElementById("new_div3").className = "fourth-row2-1";
        new_div.id = "new_div" + count;
        new_div.className = "fourth-row2-2";
    }
    new_div.appendChild(remoteVideo);
    insertCaption(count, response.peer_display_name, new_div);
    t.appendChild(new_div);
});

function insertCaption(count, name, new_div) {
    var para = document.createElement("P");
    var textnode = document.createTextNode(count + ": " + name);
    para.appendChild(textnode);
    var local_caption = document.createElement('div');
    local_caption.className = "overlay-desc";
    local_caption.appendChild(para);
    new_div.appendChild(local_caption);
}

function showChat(flag) {
    if (flag === true) {
        document.getElementById("second").style.width = "75%";
        document.getElementById("chat_div").style.display = "block";
    } else {
        document.getElementById("second").style.width = "100%";
        document.getElementById("chat_div").style.display = "none";
    }
}

function insertCaptionLocal(new_div) {
    var para = document.createElement("P");
    var textnode = document.createTextNode(count + ": " + name);
    para.appendChild(textnode);
    var local_caption = document.createElement('div');
    local_caption.className = "overlay-desc";
    local_caption.appendChild(para);
    new_div.appendChild(local_caption);
}

$(document).on("userLeftEvent", function (event, name) {
    var i;
    for (i = 1; i < video_src.length; i++) {
        if (name === video_src[i].name) {
            var tag = "#new_div" + video_src[i].serialNo;
            $(tag).remove();
            video_src.splice(i, 1);
        }
    }
    notify(name, 2);    // 2nd arg = 2 for user left
    if (count > 0) {
        count--;
    }
    adjustStyle();
});

function adjustStyle() {
    if (count === 0) {
        if (document.getElementById("new_div1")) {
            $("#new_div1").remove();
        } else if (document.getElementById("new_div2")) {
            $("#new_div2").remove();
        }
    }
    if (count === 1) {
        if (document.getElementById("new_div1")) {
            $("#new_div1").removeClass("third-row1-1");
            $("#new_div1").addClass("remote-video1");
        } else if (document.getElementById("new_div2")) {
            $("#new_div2").removeClass("third-row1-2");
            $("#new_div2").addClass("remote-video1");
            $('#new_div2').attr('id', 'new_div1');
        }
    }
}

function askConfirmation() {
    $("#second").removeClass("show");
    $("#second").addClass("none");
    $("#returnMeeting").addClass("show");
}

$(document).on("disconnectFunctionEvent", disconnectFunctionEventHandler);
function disconnectFunctionEventHandler() {
    cleanAllDom();
    //cleanGlobalVar();
    document.getElementById("askDiv").style.display = "none";
    document.getElementById("logoutDiv").style.display = "block";
}
function cleanAllDom() {
    $("#videos").empty();
    //$("#videos").remove();
    $("#localContainer").empty();
    /* original
    $("#videos").empty();
    localVideo_bottom.pause();
    localVideo_bottom.src = null;*/
    //new addition
    /*$("#videos").remove();
    $("#bottom").remove();*/
    //another attempt
    /*$("#second").remove();
    $("#first").remove();
    $("#chat_div").remove();*/
    /*
    $("#second").empty();
    $("#first").empty();
    $("#chat_div").empty();*/
}



function ReturnToMeeting() {
    $("#returnMeeting").removeClass("show");
    $("#second").removeClass("none");
}
$(document).on("notifyEvent", function (event, peerName, flag) {
    notify(peerName, flag);
});
function notify(peerName, flag) {
    var textnode;
    if (flag === 1) {
        textnode = document.createTextNode(peerName + " joined this meeting.");
    } else if (flag === 2) {
        textnode = document.createTextNode(peerName + " left this meeting.");
    } else if (flag === 3) {
        textnode = document.createTextNode("You are the only one in this meeting. Waiting for others to join");
    } else if (flag === 4) {
        textnode = document.createTextNode("You are being connected with other participants");
    }
    var para = document.createElement("P");
    para.appendChild(textnode);
    var notify_caption = document.createElement('div');
    notify_caption.id = "notify_div";
    notify_caption.className = "overlay-notification";
    notify_caption.appendChild(para);
    $("#second").append(notify_caption);
    setTimeout(removeNotification, 1000);
}

function removeNotification() {
    $("#notify_div").remove();
}

//mute/unmute mic and/or speaker
function muteUnmuteLocal(flag) {
    if (flag === true) {     // mic mute/unmute
        if (localStream.getAudioTracks()[0].enabled) {
            localStream.getAudioTracks()[0].enabled = false;
            $("#muteLocal").val("Mic Unmute");
        } else {
            localStream.getAudioTracks()[0].enabled = true;
            $("#muteLocal").val("Mic Mute");
        }
    } else {               //// speaker mute/unmute
        var i;
        for (i = 1; i < video_src.length; i++) {
            if (video_src[i].src.getAudioTracks()[0].enabled) {
                video_src[i].src.getAudioTracks()[0].enabled = false;
                $("#muteRemote").val("Speaker Unmute");
            } else {
                video_src[i].src.getAudioTracks()[0].enabled = true;
                $("#muteRemote").val("Speaker Mute");
            }
        }
    }

}

//pause self video on local and remote screens
function pauseLocal() {
    if ($("#pause").val() === "Pause") {
        localStream.getVideoTracks()[0].enabled = false;
        $("#pause").val("Play");
    } else if ($("#pause").val() === "Play") {
        localStream.getVideoTracks()[0].enabled = true;
        $("#pause").val("Pause");
    }

}

//hide local video and enlarge remote videos
function hideSelfView() {
    if ($("#hideSelf").val() === "Hide") {
        $("#bottom").removeClass("show");
        $("#bottom").addClass("none");
        $("#hideSelf").val("Show");
        $("#videos").removeClass("normal_videos");
        $("#videos").addClass("enlarged_videos");
    } else if ($("#hideSelf").val() === "Show") {
        $("#bottom").removeClass("none");
        $("#bottom").addClass("show");
        $("#hideSelf").val("Hide");
        $("#videos").removeClass("enlarged_videos");
        $("#videos").addClass("normal_videos");
    }
    $("#bottom").addClass("none");
}
