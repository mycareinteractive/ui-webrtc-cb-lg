var rootPath = window.location.pathname.split("/")[1];
var baseURL = window.location.origin + "/" + rootPath;

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		window.location = "home.html";
	} else {
		$("#wrapper").show();	
		$("#loader").show();
		$.ajax({
			method  : 'GET',
			url     : baseURL+"/rest/CNX/validate",
			headers : {'Content-Type': 'application/json'},
			async	:	false,
			statusCode	: {
				200 : function(res) {
					localStorage.setItem('validateRequ', res.validate);
					if(res.validate !== "true" ) {
						window.location = "home.html";
					}
				},
				500 : function(xhr) { 
					window.location = "home.html";
				}
			}
		}).complete(function() {
			$("#loader").hide();
		});
		
	}