var connected, 
	meaScript = false,
	testCall = false,
	authWindow,
	oldLocation = '';

jQuery(document).on('click', '.TESTButton', function() {
	
	testCall = true;
    authWindow = window.open('about:blank', '', 'left=150,top=20,toolbar=0,resizable=1,height=620,width=800');
	testConnection();
	return;
});


jQuery(document).on('click', '.LANButton', function() {
	
	var langOption = jQuery(this).attr('id');
	testCall = false;
	launchApp(langOption);
	return;
});


function meaCallbackSupport() {
	$.ajax({
		method  : 'GET',
		url     : baseURL+"/rest/CNX/meaServer",
		headers : {'Content-Type': 'application/json'},
		async	:	false,
		statusCode	: {
			200 : function(res) {
				localStorage.setItem('callbackSupport', res.validate);
				if(res.validate === "true") {				
					
					$.getScript(res.meaUrl, function(response,status) {						
						
						if(status=="success") {
							meaScript = true;
						}
					});					
				}
				
			},
			500 : function(res) {
				localStorage.setItem('callbackSupport', "false");
			}
		}
	}).complete(function() {
		
	});

}


function testConnection() {
	
	$("#loader").show();
	
	var url = window.location.href.replace(/\/[^\/]+$/, "/")+ "rest/CNX/getTestUrl/";
	
	var username, userDataURL;
	if (localStorage.getItem('username').indexOf('\\') != -1) {

		username = localStorage.getItem('username')
				.split('\\')[1];
	} else if (localStorage.getItem('username')
			.indexOf('/') != -1) {
		username = localStorage.getItem('username')
				.split('/')[1];
	} else {
		username = localStorage.getItem('username');
	}
	
	
	var ajaxState = false;
	$.ajax({
				type : 'GET',
				dataType : "json",
				url : url,
				cache : false,
				success : function(data) {
					
					if(localStorage.getItem('callbackSupport') === "false") {
						userDataURL = data.cnxURL + "?name=" + username
						+ "&address=" + username
						+ "@carenection.com&realm=anonymous"; //&autostart=true
					}
					else {
						userDataURL = data.cnxURL + "?username=" + username
							+ "&address=" + username
							+ "@carenection.com&realm=anonymous";  //&autostart=true
					}
					
					
					 //window.open(userDataURL,'_blank');
					  authWindow.location.replace(userDataURL);
					 $("#loader").hide();
				},
				error : function(jqxhr, statusCode, errorthrown) {
					$("#loader").hide();
					$("#alertDiv").show()						
					$("#alertMsg").html("Test Connection failed. Please try again");
					//$("#alertMsg").html("Connectivity issue. Please try Test Connection to ensure that you could connect to our server on video. If problem still persists please contact our administrator.");
					
				}
	});
	
	$("#loader").hide();
	
	
	
}


function launchApp(languageSelected) {
	$("#loader").show();
	
	
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
			.test(navigator.userAgent)) {
		var url = window.location.href.replace(/\/[^\/]+$/, "/")
				+ "rest/CNX/getAppUrl/" + languageSelected;
	} else {
		var url = window.location.href.replace(/\/[^\/]+$/, "/")
				+ "rest/CNX/getWebUrl/" + languageSelected;

	}
    
	var ajaxState = false;
	$.ajax({
				type : 'GET',
				dataType : "json",
				url : url,
				cache : false,
				success : function(data) {
					
					ajaxState = true;
					
					var vmrInfo = data.cnxURL.split('/'),
						vmr = vmrInfo[vmrInfo.length - 1];
					
					//$("#loader").hide();
					$("#connectionInfo").show();
					$("#room_no").val(vmr);
					
					connect();
					
					/*if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
							.test(navigator.userAgent)) {
						redirectToApp(data.cnxURL);
					} else {

						if (data.cnxURL.indexOf('?') != -1) {
							var fullURL = data.cnxURL.split('?'), newURL = fullURL[0];
						} else {
							newURL = data.cnxURL
						}

						if (localStorage.getItem('validateRequ') === "true") {
							
							var username, userDataURL;
							if (localStorage.getItem('username').indexOf('\\') != -1) {

								username = localStorage.getItem('username')
										.split('\\')[1];
							} else if (localStorage.getItem('username')
									.indexOf('/') != -1) {
								username = localStorage.getItem('username')
										.split('/')[1];
							} else {
								username = localStorage.getItem('username');
							}
							
							if(localStorage.getItem('callbackSupport') === "false") {
								userDataURL = newURL + "?name=" + username
								+ "&address=" + username
								+ "@carenection.com&realm=anonymous"; //&autostart=true
							}
							else {
								userDataURL = newURL + "?username=" + username
									+ "&address=" + username
									+ "@carenection.com&realm=anonymous";  //&autostart=true
							}
							
							redirectToApp(userDataURL);
						} else {
							
							var userDataURL = newURL;
							redirectToApp(userDataURL);
						}
						
						
					}

					window.focus();*/
				},
				error : function(jqxhr, statusCode, errorthrown) { 
					debugger;
					ajaxState = true;
					$("#loader").hide();
					if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
							.test(navigator.userAgent)) {
						alert("Conference creation failed");
					}
					else
					{
						$("#alertDiv").show()						
						$("#alertMsg").html("Conference creation failed");
					}						
			
				}
			}).complete( function(jqxhr){	
				if(!ajaxState) {
					//$("#loader").hide();
					if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
							.test(navigator.userAgent)) {
						alert("Conference creation failed");	
					}
					else
					{
						$("#alertDiv").show()
						$("#alertMsg").html("Conference creation failed");
					}			
					
				}
			});
}


function meaInitializedHandler(meaEvent) {
	 	 
    Polycom.MEA.on("UI.ConnectionStateChanged", meaConnectionStateChanged);
}

function meaConnectionStateChanged(meaEvent) {
 	
	if(testCall === false)
	{
 		if (localStorage.getItem('validateRequ') !== "true") {
		
			if (meaEvent.Value.state === "connected")
			{
				$("#loader").hide();
	    		$("#iframeDiv").show();
	            $("#iframeContainer").fadeIn("slow");
	            
	            connected = true;
			}
			 else if (meaEvent.Value.state === "ended") { 
		        $("#iframeContainer").fadeOut("slow");
		        $("#iframeDiv").hide();      
		        $("#mea_iframe").attr("src", "");
		        
		     }
			
		}
		
		else {
			
			if (meaEvent.Value.state === "meeting") { 
		    	
		    	connected = true;
		    	
		    	setTimeout(function() {
		    		$("#loader").hide();
		    		$("#iframeDiv").show();
		            $("#iframeContainer").fadeIn("slow");
		    	}, 2000);
		    	
		        
		    }
		    else if (meaEvent.Value.state === "ended") { 
		        $("#iframeContainer").fadeOut("slow");
		        $("#iframeDiv").hide(); 
		        $("#mea_iframe").attr("src", "");
		        
		     }
			
		}
	}
	
    
}

function connectionTimeOut() {
	
	if(connected === false)
	{
		$("#loader").hide();
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
				.test(navigator.userAgent)) {
			alert("Connectivity issue. Please try again.");	
		}
		else
		{
			$("#iframeContainer").fadeOut("slow");
	        $("#iframeDiv").hide(); 
	        $("#mea_iframe").attr("src", "");
			$("#alertDiv").show();
			//$("#alertMsg").html("Connectivity issue. Please try again.");
			$("#alertMsg").html("Connectivity issue. Please try clicking on Setup & Verify connection to ensure all prerequisites are installed for video calls. If problem still persists please contact administrator.");
		}
	}
}


function redirectToApp(launchLink) {
	oldLocation = window.location.href;
	
	
	if (navigator.userAgent.match(/(iPhone|iPad);?/i)
			|| navigator.userAgent.match(/android/i)) {

		window.open(launchLink, '_self');

		if (launchLink.indexOf("martti://") == -1) {
			window.setTimeout(function() {
				window.location = oldLocation;
			}, 5111);
		} else {

			window.setTimeout(function() {
				window.location = oldLocation;
			}, 99999);
		}

	} else {
		// check for call back support of Connection Status from Cloud Axis - meetme.carenection.com (V 2.0.2)
		 if(localStorage.getItem('callbackSupport') === "true")
		 {
			 // check for CA SSL certificate issue
			 if(meaScript === true) {
				 connected = false;
				 setTimeout(connectionTimeOut, 60000);
				 $("#mea_iframe").attr("src", launchLink);
			 }
			 else {
				 connected = true;
				 window.location = launchLink;
			 }
			 
			 
		 }
		 else //  Cloud Axis - go.carenection.com version 
		 {
			 $("#loader").hide();
			 $("#mea_iframe").attr("src", launchLink);
			 $("#iframeContainer").fadeIn("slow");
		     $("#iframeDiv").show(); 
		     connected = true;
			 
		 }
			 
	}
}
 function loginRequired() {
	 $.ajax({
			method  : 'GET',
			url     : baseURL+"/rest/CNX/validate",
			headers : {'Content-Type': 'application/json'},
			async	:	false,
			statusCode	: {
				200 : function(res) {
					localStorage.setItem('validateRequ', res.validate);
				},
				500 : function(xhr) { 
					localStorage.setItem('validateRequ', "false");
				}
			}
		});
 }

$(document).ready(function() {	
	app.init();
});

var rootPath = window.location.pathname.split("/")[1],
	baseURL = window.location.origin + "/" + rootPath,
	indexDBSupport = true;
var app = {
	init : function() {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			$("#headerContainer").hide();
			$("#loader").hide();
		} else {
			 
			 window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;			
			
			 if (!window.indexedDB) {
				 indexDBSupport = false;				 
			 } else {
				 indexDBSupport = true;
			 }
			 
			//loginRequired();
			//meaCallbackSupport();
			/*$(document).on("click", "#fullScreen", toggleFullScreen);
			$(document).on("click", "#closeIcon", function() {
				$("#iframeContainer").fadeOut("slow");
		        $("#iframeDiv").hide(); 		        
		        $("#mea_iframe").attr("src", "");
			});
			 
			if (localStorage.getItem('validateRequ') !== "true") {
				$("#headerContainer").hide();
				$("#loader").hide();
				
			} else { 
				
				if(localStorage.getItem('login') != "yes"  && indexDBSupport === true ) {
					$("#loader").show();
					setTimeout("getUser('home')", 3000);
					
				}
				else {	
					localStorage.removeItem('login');
					if(localStorage.getItem('username') !== null) {
						if(localStorage.getItem('username').indexOf('\\') == -1) {
							var displayName = localStorage.getItem('username');
						} else {
							var dpName = localStorage.getItem('username').split("\\"),
							    displayName = dpName[1];
						}
						$(".uname").html(displayName);
					}
					$("#loader").hide();
					
				}
				
				
				$("#headerContainer").show();
				$(".logImg").on("click", app.logout);
				
			}
			
			*/
			 $("#okBtn, #alertContainer").on('click', function() {
					$("#alertDiv").hide();
				});
		}
		//$("#loader").hide();
		if(localStorage.getItem('iphone') !== "true") {
			app.autoFit();
			$(window).resize(app.autoFit);
		}
		
		
	},
	autoFit : function() { 
		$("#wrapper").height(window.innerHeight + "px");
		$("#loader").height(window.innerHeight + "px");
	},
	auth : function(uname, pword) {

		var postData = {
			"username" : uname,
			"password" : pword
		}
		$.ajax({
			method : 'POST',
			url : baseURL + "/rest/CNX/user/auth",
			data : JSON.stringify(postData),
			headers : {
				'Content-Type' : 'application/json'
			},
			statusCode : {
				200 : function(res) {
					localStorage.setItem('username', uname);
					if(localStorage.getItem('username').indexOf('\\') == -1) {
						var displayName = localStorage.getItem('username');
					} else {
						var dpName = localStorage.getItem('username').split("\\"),
						    displayName = dpName[1];
					}
					$(".uname").html(displayName);
					
					$("#loader").hide();
				},
				401 : function(xhr) {
					$("#loader").hide();
					$("#alertDiv").show()
					$("#alertMsg").html("Unauthorized user");
					window.location = "index.html";
				},
				500 : function(xhr) {
					$("#loader").hide();
					$("#alertDiv").show()
					$("#alertMsg").html("Network connectivity issue");
					window.location = "index.html";
				}
			}

		});
	},
	logout : function() {
		localStorage.clear();
		remove();
		window.location = "index.html";
	}
};
