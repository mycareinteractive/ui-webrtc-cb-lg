$(document).ready(function() {	
		app.init();
	});
	
	//var baseURL = window.location.href.replace(/\/[^\/]+$/, "/"),
	var baseURL = "https://demo.carenection.com:8443/Martti_Cleveland/",	
		meetingURL = '',
		assignedVMR = '',
		intervalRef,
		phoneNumber;
	var app = {
		init : function(){
			$("#loader").show();
			app.fetchVMR();	
			$(".inviteMenu ul li").click(app.menuClick.bind(this));
			$(".sendInvite").click(app.sendInvite.bind(this));
			$(".decline").click(app.declineCall);
			$(".join").click(app.joinCall);
			
		},		
		/* Dynamically create your VMR */
		fetchVMR : function() {
			$.ajax({
				type : 'GET',
				dataType : "json",
				url : baseURL+"rest/CNX/getWebUrl/Spanish",
				cache : false,
				
				success : function(data) {
					meetingURL = data.cnxURL;
					var extractVMR = data.cnxURL.split('/');
					assignedVMR = extractVMR[extractVMR.length - 1];
					$("#loader").hide();
					$(".assingedVMR").html(assignedVMR);
					$("#room_no").val(assignedVMR);
					
					intervalRef = setInterval(app.pollPariticipant, 10000);
				},
				error: function(e) {
				    $("#loader").hide();
					alert("Conference creation failed");					
		        }
				
			});
		},
		/* Send Invite to recipient via Email, Phone */
		sendInvite : function(ele) {
			var mobile='', email='', videoConfURL='';
			if(ele.target.attributes[0].nodeValue=="phone") {
				mobile = $("#mobile").val();
				//videoConfURL = "martti://ClevelandClinic?dialString="+assignedVMR+"@join.carenection.com";
				videoConfURL = assignedVMR;
				if(mobile=="") {
					alert("Enter Invitee Phone Number");
					return false;
				}
				app.phoneNumber = mobile;
			}
			if(ele.target.attributes[0].nodeValue=="email") {
				email = $("#email").val();
				videoConfURL = meetingURL;
				if(email=="") {
					alert("Enter Invitee Email Address");
					return false;
				}
			}						
			$("#loader").show();
			/*var postData =  {								
								"email": email,
								"phoneNumber": mobile,
								"language": "Spanish",
								"meetingURL": videoConfURL
							};*/
			
			var postData = "email="+email+"&phoneNumber="+encodeURIComponent(mobile)+"&language=Spanish&meetingURL="+videoConfURL;
			
			//$("#reqRes").append("<br> Request: "+postData);
			
			$.ajax({
				method  : 'GET',
				url     : baseURL+"rest/CNX/sendinvite?"+postData,				
				/*headers : {					
					'Content-Type': 'application/json'
				},*/
				success : function(data) {					
					$("#loader").hide();
					//$("#reqRes").append("<br> Response"+JSON.stringify(data)).append(JSON.stringify(this.crossDomain));
                    app.info("Invitation to " + app.phoneNumber + " sent successfully.");
				},
				error: function(e) {
					//$("#reqRes").append("<br> Response"+JSON.stringify(e));
					$("#loader").hide();
					//console.log(this.headers);
                    app.warn("Send Invite failed. Please try again with valid information.");
		        }				
			});
		},	
		/* Poll for participant joining your VMR*/
		pollPariticipant : function() {
			var self = this;
			$.ajax({
				method  : 'GET',
				url     : baseURL+"rest/CNX/conferences/"+assignedVMR+"/participants",
				/*headers : {'Content-Type': 'application/json'},*/
				success : function(data) {
					console.log("New participants found.");						
					if(data["cnx-participants-count"] > 0) {
						$("#inviteName").html(data["cnx-participants"]["participant-name"] + ' ' + app.phoneNumber);
						app.displayCallAlert();
					}
					
				},
				error: function(e) {					
					console.log("No participants found.");					
		        }				
			});
		},
		/* Decline the call */
		declineCall : function () {
            app.hideCallAlert();
		},
		/* Join the call */
		joinCall : function () {
			clearInterval(intervalRef);
            app.hideCallAlert();
			connect();
		},
		menuClick: function(ele) {
			$(".inviteMenu ul li").removeClass("inviteMenuSelected");
			$(ele.target).addClass("inviteMenuSelected");
			$(".inviteContent>div").hide();
			$("#"+ele.target.attributes[0].nodeValue).show();
			focusButton($("#"+ele.target.attributes[0].nodeValue+' button').first());
		},
		displayCallAlert: function() {
            $("#callAlert").show(500);
            focusButton($("#callAlert button").first());
		},
		hideCallAlert: function() {
			blurButton();
            $("#callAlert").hide(500);
		},
		info: function(msg) {
			$('#tab2>div.text, #tab3>div.text').html(msg);
		},
		warn: function(msg) {
            $('#tab2>div.alert, #tab3>div.alert').html(msg);
		}

		
	};