        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.GetUserMedia || navigator.msGetUserMedia;
        windowURL = window.URL || window.webkitURL || window.mozURL || window.msURL;
		 var isConnected = false;
        var request = null;
        var hangingGet = null;
        var localName;
        var server;
        var my_id = -1;
        var g_peer_id = -1;
		var peerid_others;
        var other_peers = [];
        var message_counter = 0;
        var localVideo;
        var remoteVideo;
        var localStream;
        var started = false; // start!!
        var signed_In = false;
        var mediaTrackConstraints = {
            'has_audio': true,
            'has_video': true
        };
        var notMultiSession = true;
        var activeCall = false;
        var connectedPeerName;
        var connectedPeerId;
        var onlyASCII = true;

        var localConstraints = {
            audio: true,
            video: {
                mandatory: {
                    minWidth: 640,
                    minHeight: 480
                }
            }
        };
        var mediaConstraints = {
            'mandatory': {
                'OfferToReceiveAudio': true,
                'OfferToReceiveVideo': true
            }
        };
        var sdpConstraints = {
            optional: [{
                DtlsSrtpKeyAgreement: true
            }, {
                RtpDataChannels: true
            }]
        };

        function startPreviewVideo() {
			//logger(" startPreviewVideo " ,"","","");
            trace("hcap.webrtc.startPreviewVideo({});");
            hcap.webrtc.startPreviewVideo({
                "onSuccess": function() {
                    trace("onSuccess to call startPreviewVideo");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function stopPreviewVideo() {
			//logger(" stopPreviewVideo " ,"","","");
            trace("hcap.webrtc.stopPreviewVideo({});");
            hcap.webrtc.stopPreviewVideo({
                "onSuccess": function() {
                    trace("onSuccess to call stopPreviewVideo");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function incomingCall() {
            var localId = 11;
            var remoteId = 13;
            var remoteSDP = "sdp message";
            trace("hcap.webrtc.incomingCall({\"localId\":" + localId + ",\"remoteId\":" + remoteId + ",\"remoteSDP\":\"" + remoteSDP + "\"});");
            hcap.webrtc.incomingCall({
                "localId": localId,
                "remoteId": remoteId,
                "remoteSDP": remoteSDP,
                "onSuccess": function() {
                    trace("onSuccess to call incomingCall");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function outgoingCallButton() {
            var localId = 11;
            var remoteId = 13;
            trace("hcap.webrtc.outgoingCall({\"localId\":" + localId + ",\"remoteId\":" + remoteId + "\"});");
            hcap.webrtc.outgoingCall({
                "localId": localId,
                "remoteId": remoteId,
                "onSuccess": function() {
                    trace("onSuccess to call outgoingCall");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function endCall() {
            trace("hcap.webrtc.endCall({});");
            hcap.webrtc.endCall({
                "onSuccess": function() {
                    trace("onSuccess to call endCall");
					//logger(" endcall to hcap success " ,"","","");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
					//logger(" endcall to hcap failed " ,"","","error  : "+f.errorMessage);
                }
            });
        }
        
        function startCheckAudioStep1() {
            trace("hcap.webrtc.startCheckAudio('microphone');");
            hcap.webrtc.startCheckAudio({
				"type":"microphone",
                "onSuccess": function() {
                    trace("onSuccess to call startCheckAudio microphone");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
		
		function startCheckAudioStep2() {
            trace("hcap.webrtc.startCheckAudio('speaker');");
            hcap.webrtc.startCheckAudio({
				"type":"speaker",
                "onSuccess": function() {
                    trace("onSuccess to call startCheckAudio speaker");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function stopCheckAudio() {
            trace("hcap.webrtc.stopCheckAudio({});");
            hcap.webrtc.stopCheckAudio({
                "onSuccess": function() {
                    trace("onSuccess to call stopCheckAudio");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function setConfigurationTurn() {
            var keyStr = "ice_cfg";
            //var valueStr = "stun:stun.l.google.com:19302;turn:89.108.113.194:3478|webrtc|webrtc;";
            //var valueStr = "stun:91.142.94.167:3478;turn:91.142.94.167:3478|webrtc|webrtc;turn:91.142.94.167:3478|andy|andy;turn:91.142.94.167:3478|serg|serg";
			 //var valueStr = "stun:91.142.94.167:3478;turn:91.142.94.167:3478?transport=tcp|webrtc|webrtc;turn:91.142.94.167:3478?transport=tcp|andy|andy;turn:91.142.94.167:3478?transport=tcp|serg|serg;";
		    //	 var valueStr = "stun:91.142.94.167:3478;turn:10.178.84.215:3478?transport=tcp|psshin|psshin;turn:10.178.84.215:3478?transport=tcp|idt|idt;turn:10.178.84.215:3478?transport=tcp|commercial|commercial;";
 		    //	 var valueStr = "stun:91.142.94.167:3478;turn:10.178.84.215:3478?transport=udp|psshin|psshin;turn:10.178.84.215:3478?transport=udp|idt|idt;turn:10.178.84.215:3478?transport=udp|commercial|commercial;";
 		    //	 var valueStr = "stun:91.142.94.167:3478;turn:10.178.84.215:3478|psshin|psshin;turn:10.178.84.215:3478|idt|idt;turn:10.178.84.215:3478|commercial|commercial;";
		    // 	 var valueStr = "stun:77.72.169.156:3478;turn:10.178.84.215:3478|psshin|psshin;turn:10.178.84.215:3478|idt|idt;turn:10.178.84.215:3478|commercial|commercial;";
/* modified for socket 			
			var valueStr = "stun:10.178.94.176:3478;turn:10.178.94.176:3478|psshin|psshin;turn:10.178.94.176:3478|idt|idt;turn:10.178.94.176:3478|commercial|commercial;";	
			*/
			var valueStr = "stun:stun.l.google.com:19302;turn:10.178.94.176:3478|psshin|psshin;turn:10.178.94.176:3478|idt|idt;turn:10.178.94.176:3478|commercial|commercial;";	
			
            trace("hcap.webrtc.setConfiguration({\"key\":\"" + keyStr + "\",\"value\":\"" + valueStr + "\"});");
            hcap.webrtc.setConfiguration({
                "key": keyStr,
                "value": valueStr,
                "onSuccess": function() {
                    trace("onSuccess to call setConfiguration");
					//logger(" setConfiguration success" ,"","","");
					
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
					//logger(" setConfiguration onFailure" ,"","","error : " + f.errorMessage);
                }
            });
        }
		
		function setConfigurationAudioConf() {
            var keyStr = "audio_streams_cfg";
            var valueStr = "googEchoCancellation|googEchoCancellation2";
            trace("hcap.webrtc.setConfiguration({\"key\":\"" + keyStr + "\",\"value\":\"" + valueStr + "\"});");
            hcap.webrtc.setConfiguration({
                "key": keyStr,
                "value": valueStr,
                "onSuccess": function() {
                    trace("onSuccess to call setConfiguration");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function getConfiguration() {
            trace("hcap.webrtc.getConfiguration({});");
            hcap.webrtc.getConfiguration({
                "onSuccess": function(s) {
                    trace("onSuccess : configurations = " + s.configurations);
                    return s.configurations;
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
            
        }
        
        function showDiagnostics() {
            trace("hcap.webrtc.showDiagnostics({});");
            hcap.webrtc.showDiagnostics({
                "onSuccess": function() {
                    trace("onSuccess to call showDiagnostics");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        function acceptMessage() {
            var remoteId = 1;
            var rtcMsg = "v=0\r\no=- 784574930202939924 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE audio video\r\na=msid-semantic: WMS TMHQQzj7s7PaqtYNaZgsHn1eeRnEYgC4sbbA\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:ZRB6aIsVSesuO15J\r\na=ice-pwd:1pdEHK5+vMcz+zdxc2fmKoRd\r\na=fingerprint:sha-256 43:6B:69:27:8A:C3:E0:1C:FC:CD:34:37:A8:A3:63:08:C5:7E:95:C5:DE:5F:7D:1A:8C:61:02:F1:EA:77:48:8B\r\na=setup:actpass\r\na=mid:audio\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=sendrecv\r\na=rtcp-mux\r\na=rtpmap:111 opus/48000/2\r\na=fmtp:111 minptime=10; useinbandfec=1\r\na=rtpmap:103 ISAC/16000\r\na=rtpmap:104 ISAC/32000\r\na=rtpmap:9 G722/8000\r\na=rtpmap:0 PCMU/8000\r\na=rtpmap:8 PCMA/8000\r\na=rtpmap:106 CN/32000\r\na=rtpmap:105 CN/16000\r\na=rtpmap:13 CN/8000\r\na=rtpmap:126 telephone-event/8000\r\na=maxptime:60\r\na=ssrc:2106420173 cname:adTsXktLU7uPdxCy\r\na=ssrc:2106420173 msid:TMHQQzj7s7PaqtYNaZgsHn1eeRnEYgC4sbbA 1d460174-d982-47b0-ad88-dd78c822011f\r\na=ssrc:2106420173 mslabel:TMHQQzj7s7PaqtYNaZgsHn1eeRnEYgC4sbbA\r\na=ssrc:2106420173 label:1d460174-d982-47b0-ad88-dd78c822011f\r\nm=video 9 UDP/TLS/RTP/SAVPF 100 116 117 96\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:ZRB6aIsVSesuO15J\r\na=ice-pwd:1pdEHK5+vMcz+zdxc2fmKoRd\r\na=fingerprint:sha-256 43:6B:69:27:8A:C3:E0:1C:FC:CD:34:37:A8:A3:63:08:C5:7E:95:C5:DE:5F:7D:1A:8C:61:02:F1:EA:77:48:8B\r\na=setup:actpass\r\na=mid:video\r\na=extmap:2 urn:ietf:params:rtp-hdrext:toffset\r\na=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=extmap:4 urn:3gpp:video-orientation\r\na=sendrecv\r\na=rtcp-mux\r\na=rtpmap:100 VP8/90000\r\na=rtcp-fb:100 ccm fir\r\na=rtcp-fb:100 nack\r\na=rtcp-fb:100 nack pli\r\na=rtcp-fb:100 goog-remb\r\na=rtpmap:116 red/90000\r\na=rtpmap:117 ulpfec/90000\r\na=rtpmap:96 rtx/90000\r\na=fmtp:96 apt=100\r\na=ssrc-group:FID 3921613324 3494539976\r\na=ssrc:3921613324 cname:adTsXktLU7uPdxCy\r\na=ssrc:3921613324 msid:TMHQQzj7s7PaqtYNaZgsHn1eeRnEYgC4sbbA 38920097-f5c1-4640-b4a7-a8f69b3d028f\r\na=ssrc:3921613324 mslabel:TMHQQzj7s7PaqtYNaZgsHn1eeRnEYgC4sbbA\r\na=ssrc:3921613324 label:38920097-f5c1-4640-b4a7-a8f69b3d028f\r\na=ssrc:3494539976 cname:adTsXktLU7uPdxCy\r\na=ssrc:3494539976 msid:TMHQQzj7s7PaqtYNaZgsHn1eeRnEYgC4sbbA 38920097-f5c1-4640-b4a7-a8f69b3d028f\r\na=ssrc:3494539976 mslabel:TMHQQzj7s7PaqtYNaZgsHn1eeRnEYgC4sbbA\r\na=ssrc:3494539976 label:38920097-f5c1-4640-b4a7-a8f69b3d028f\r\n";
            trace("hcap.webrtc.acceptMessage({\"remoteId\":" + remoteId + ",\"rtcMsg\":\"" + rtcMsg + "\"});");
            hcap.webrtc.acceptMessage({
                "remoteId": remoteId,
                "rtcMsg": rtcMsg,
                "onSuccess": function() {
                    trace("onSuccess to call acceptMessage");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }


        function AsciiHex2Str(sAsciiHex) {
            var sRes = "";
            if ((sAsciiHex.length > 0) && (sAsciiHex.length % 2 == 0)) {
                var items = sAsciiHex.match(/.{2}/g);
                for (var i = 0; i < items.length; i++) {
                    var iItem = parseInt(items[i], 16);
                    sRes += String.fromCharCode(iItem);
                }
            }
            return sRes;
        }

        function str2AsciiHex(sStr) {
            var sRes = "";
            for (var i = 0; i < sStr.length; i++) {
                sRes += sStr.charCodeAt(i).toString(16).toUpperCase();
            }
            return sRes;
        }

        function randomString() {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var string_length = 4;
            var randomstring = '';
            for (var i = 0; i < string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
            }
            //document.getElementById("local").value = "SHINTV-Pyeongtaek " + randomstring;
            document.getElementById("local").value = "SHINTVSeochoLab" + randomstring;
        }

        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        function AsciiCodes2Str(sAscii) {
            var sRes = "";
            sAscii = sAscii.replace('"', '');
            var items = sAscii.split(';');
            for (var i = 0; i < items.length; i++) {
                if (!isNumber(items[i])) {
                    break;
                }
                var iItem = parseInt(items[i]);
                sRes += String.fromCharCode(iItem);
            }
            return sRes;
        }

        function str2AsciiCodes(sStr) {
            var sRes = "";
            for (var i = 0; i < sStr.length; i++) {
                sRes += sStr.charCodeAt(i) + ';';
            }
            return sRes;
        }

		function trace(txt) {
            console.log("["+ Date.now() +"]"+txt);
       }
 		function trace_rt(txt) {
            console.log("["+ Date.now() +"]"+txt);
       }       
        function clearDebug() {
            document.getElementById("debug").innerHTML = "";
       }

        function keydown(e) {
            if (!e) e = event;

            var code = (e.keyCode ? e.keyCode : e.which);

            switch (code) {
                case hcap.key.Code.UP:
                case hcap.key.Code.DOWN:
                case hcap.key.Code.ENTER:
                case hcap.key.Code.PORTAL:
                case hcap.key.Code.EXIT:
            }
        }

        function init_key_listener() {
            if (document.addEventListener) {
                document.addEventListener("keydown", keydown, false);
            } else if (document.attachEvent) {
                document.attachEvent("onkeydown", keydown);
            }
        }

        function sendMessage(peer_id, message) {
            trace("[sendMessage] >>>");
			
            trace("[sendMessage] peer_id:" + peer_id + " C->S: " + message);
			
			//logger(" sending sdpevent" ,"","","");
            /*var xhr = new XMLHttpRequest();
            xhr.open('POST', server + "/message/peer_id/" + my_id + "/to/" + peer_id, true);
            xhr.setRequestHeader("Content-Type", "text/plain");
            xhr.send(message);
            xhr = null;*/
			if(message){
				$(document).trigger("sdpEvent",[message,peer_id]);
				
			}
        }

        function setLocalAndSendMessage(sessionDescription) {
            trace("[setLocalAndSendMessage]");
            //   trace("[setLocalAndSendMessage] SET LOCAL SDP ");
            //pc.setLocalDescription(sessionDescription);

            //   sendMessage(sessionDescription);
        }

        var CallAnswerErrorCallBack = function(e) {
            trace("Something wrong happened when answer or offer " + e.toString());
        };

        function doAnswer(peer_id, sdp_msg) {            
            trace("[doAnswer] SEND INCOMING CALL ");
            trace("signed_In = " + signed_In + ", started = " + started + ", activeCall = " + activeCall + ", my_id = " + my_id + ", peer_id = " + peer_id);
			//logger(" doAnswer" ,"","","sdp_msg = "+sdp_msg);
            hcap.webrtc.incomingCall({
                "localId": my_id,
                "remoteId": peer_id,
                "remoteSDP": sdp_msg,
                "onSuccess": function() {
                    trace("onSuccess  incomingCall");
					//logger(" incomingCall success" ,"","","");
                    g_peer_id = peer_id;
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                    g_peer_id = -1;
					//logger(" onFailure incomingCall" ,"","","errorMessage = " + f.errorMessage);
                }
            });

            activeCall = true;
            started = true; // ���⼭ �ɰ� ���� �Ŀ��� false�� ��
        }

		function sleep(gap){ /* gap is in millisecs */ 
		  var then,now; 
		  then=new Date().getTime(); 
		  now=then; 
		  while((now-then)<gap){ 
		    now=new Date().getTime();  // ����ð��� �о� �Լ��� �ҷ����� �ð����� ���� �̿��Ͽ� ó�� 
		  } 
		} 
		 //check offer or answer first
		 var messageBuf= new Array();

        function processSignalingMessage(message, peer_id) {
            //trace('processSignalingMessage:' + message);
			//logger(" --response---processSignalingMessage msg received from server" ,"","started  = "+ started,"message = "+ message.message_type);
            try {
            	if (Object.keys(message).length <= 0) {
            		//trace("processSignalingMessage : message.length[" + message.length + "] <= 0");
            		return;
            	}
				//var msglist = JSON.parse(message);
				var msg = message;
            	//trace("msglist length = " + msglist+'started :'+started);

				var foundflag = 0;
			  	//for (var k = 0; k < msglist.length; k++) {
			  	   //var msg = msglist[k];
                 //var msg_encoded = JSON.stringify(msg);
				    if (msg.message_type === 'offer') {
						var sdp_obj={ 
									'type':msg.message_type,
									'sdp' :msg.sdp					
						}
						var msg_encoded = JSON.stringify(sdp_obj);
				        doAnswer(peer_id, msg_encoded);
				        //sleep(200);
				        foundflag =1;
					//logger(" --response-- receiving sdpevent offer" ,"","","sdp = "+msg.sdp);	
				    } else if (msg.message_type === 'answer' && started) {
					//	logger("processSignalingMessage answer received" ,"","","message"+msg);
				        trace("processSignalingMessage msg.type=" + msg.message_type);
						 trace('[processSignalingMessage] SEND ACCEPT MESSAGE');
						var sdp_obj={ 
									'type':msg.message_type,
									'sdp' :msg.sdp					
						}
						var msg_encoded = JSON.stringify(sdp_obj);
	                    hcap.webrtc.acceptMessage({
	                        "remoteId": peer_id,
	                        "rtcMsg": msg_encoded,
	                        "onSuccess": function() {
	                            trace("onSuccess to call incomingCall");
								//logger(" onSuccess acceptMessage" ,"","","");
							//	logger("processSignalingMessage answer received success" ,"","","");
	                        },
	                        "onFailure": function(f) {
	                            trace("onFailure : errorMessage = " + f.errorMessage);
								//logger(" onFailure acceptMessage" ,"","","errorMessage = " + f.errorMessage);
	                        }
	                    });
				        foundflag = 1;
						//logger(" --response-- receiving sdpevent answer" ,"","","sdp = "+msg.sdp);	
				    }  else if (msg.message_type === 'end_media_session' && started) { // started�� ���� ��������
							isConnected = false;
							onRemoteHangup();
							foundflag = 1;
				    } 
					if(msg.message_type === 'solicit_offer') {
						for(i = 1; i <= Object.keys(other_peers).length; i++) {
							if(msg.peer_display_name === other_peers[i].name) {
								var obj_mod = {
									name : msg.peer_display_name,
									call_id : msg.call_id,
									conf_id : msg.conference_id,
									med_id : msg.media_session_id,
									txn_id : msg.transaction_id
								}
							other_peers[i]= obj_mod;
							outgoingCall(i);
							break;
							}
						}
					}	
					if (msg.message_type === "keep_alive") {
						
						$(document).trigger("keep_aliveEvent",[msg]);
        
					}
    
			  	/*if ( foundflag === 0 && activeCall === false )
			  	{
			  		 trace("store message in buffer until get offer or answer message");
			  		 messageBuf.push(msg);
			  		 return;			  		
			  	}*/
			  	
				//do {
	            	//for (var j = 0; j < msglist.length; j++) {
		                //var msg = msglist[j];
		                //var msg_encoded = JSON.stringify(msg);
		
		                if (msg == null) {
		                    trace('processSignalingMessage: msg == null');
		                    return;
		                }
		
		                trace("processSignalingMessage [krutogolov] [" + msg.message_type + "] "+ msg);
		                
		                if (msg.message_type === 'offer') {                    
		                    trace('offer already processed skip here');
		                } else if (msg.message_type === 'answer' && started) {
		                    trace("answer already processed skip here");		                 	
		                } else if (msg.message_type === 'ice_candidate' && started && msg.ice_candidate!=null ) {				
		                    trace('[processSignalingMessage] CREATE ICE CANDIDATE AND ADD ICE CANDIDATE');
							var candidate_obj={ 
									
										   "candidate" :msg.ice_candidate.candidate ,
										   "sdpMLineIndex" :msg.ice_candidate.sdpMLineIndex ,
										   "sdpMid" : msg.ice_candidate.sdpMid,
										   "type" : "candidate"
													
										}
						var msg_encoded = JSON.stringify(candidate_obj);
						   hcap.webrtc.acceptMessage({
		                        "remoteId": peer_id,
		                        "rtcMsg": msg_encoded,
		                        "onSuccess": function() {
		                            trace("onSuccess to call incomingCall");
									//logger(" onSuccess acceptMessage candidates" ,"","","");
		                        },
		                        "onFailure": function(f) {
		                            trace("onFailure : errorMessage = " + f.errorMessage);
									//logger(" onSuccess acceptMessage candidates" ,"","","error :"+f.errorMessage);
		                        }
		                    });
						//logger(" --response--receiving sdpevent candidate" ,"","","sdp = "+msg_encoded);	
		                } 
	            	
					//var endofList = true;
					/*if(messageBuf.length != 0 )
					{ msglist = messageBuf.pop();
					  trace('msglist:'+JSON.stringify( msglist));
					  trace('after pop, messageBug.length: '+messageBuf.length);
					  endofList = false;
					}*/
				
				//}while(endofList != true )            	
            	//*/
            } catch (e) {
                trace('processSignalingMessage e:' + e.message());
            }

        }

        function handlePeerMessage(peer_id, data) {
            //trace("[handlePeerMessage] peer " + peer_id + " data " + data);
		//	logger(" handlePeerMessage " ,"","",data.message_type);
            processSignalingMessage(data, peer_id);
        }

        function clearOtherPeers() {
            var peerKeys = Object.keys(other_peers);
            for (i = 0; i < peerKeys.length; i++) {
                updatePeers(false, peerKeys[i], "");
            }
        }

        function disconnect() {
            trace_rt("[disconnect] id:" + my_id);
            
            endCall();
            /*
            if (request) {
                request.abort();
                request = null;
            }

            if (hangingGet) {
                hangingGet.abort();
                hangingGet = null;
            }
			
            if (my_id != -1) {
                request = new XMLHttpRequest();
                request.open("GET", server + "/sign_out/peer_id/" + my_id, true);
                request.send();
                request = null;
                my_id = -1;
            }
			*/
			$(document).trigger("confirmDisconnectEvent");
			//sleep(200);
          
		//	document.getElementById("server").disabled = false;
        //    document.getElementById("local").disabled = false;
   		     g_peer_id = -1;
            activeCall = false;
            signed_In = false;
            clearOtherPeers();
            showSignInStatus();
        }

        function GetIntHeader(r, name) {
            var val = r.getResponseHeader(name);
            return val != null && val.length ? parseInt(val) : -1;
        }

        function callThisPeer(peer_id) {
            trace("[callThisPeer]");
			//$(document).trigger("connectEvent");
			if(other_peers[peer_id].med_id) {
				outgoingCall(peer_id);
			} else {
				$(document).trigger("connectEvent");
			}
            
        }

        function createPeerToCall(name, peerId, elem) {
            trace("[createPeerToCall] name=" + name);

            var tabIndexValue = document.querySelectorAll("button").length;
            var table = document.getElementById("peersLog");
            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
            row.setAttribute("name", "peer");
            row.setAttribute("id", "peer" + peerId);
            var newcell0 = row.insertCell(0);
            newcell0.style["witdh"] = "60px";
            newcell0.setAttribute("id", "buttons");
            newcell0.setAttribute("class", "peerInfo");
            trace("[createPeerToCall] activeCall=" + activeCall);
            if (!activeCall) {
                newcell0.innerHTML = '<button name="call" class="btn btn-primary " id="' + peerId + '" onclick="callThisPeer(this.id)" tabindex="'+tabIndexValue+'"><img id="imgCall" src="./phone-answer-green-th.png"/></button>';
                            trace("[createPeerToCall] create call button=" + name + peerId);
            } else {
               // newcell0.innerHTML = '<button name="call" class="btn btn-warning " id="' +  peerId + '" onclick="callThisPeer(this.id)" disabled=true><img id="imgCall" src="./phone-answer-gray-th.png"/></button>';
            }
            var newcell1 = row.insertCell(1);
            newcell1.setAttribute("id", "info");
            newcell1.setAttribute("class", "peerName");
           // newcell1.innerHTML = '<b>' + "<pre>"+ "  "  + name +  "</pre>" +'</b>';
           newcell1.innerHTML = '<b>' + "&nbsp" + name + "&nbsp" + "&nbsp" + peerId + '</b>';
        }

        function updatePeers(add, peerId, name) {
            trace("[updatePeers]" + add + ", peer " + peerId + ", name " + name);
         //   var peerView = document.getElementById("peersLog");
            if (add) {
                var peerKeys = Object.keys(other_peers);
                if (peerKeys.length == 0) {
                  //  peerView.innerHTML = "";
                }
                if (onlyASCII)
                    var decodeName = name;
                else
                    var decodeName = decode_utf8(name);
                trace("name = " + name + " decodeName = " + decodeName);
                //other_peers[peerId] = decodeName;
				var obj = {
					name : name,
					call_id : "",
					conf_id : "",
					med_id : "",
					txn_id : ""
				}
				other_peers[peerId]= obj;
               // createPeerToCall(decodeName, peerId, peerView);
            } else {
                delete other_peers[peerId];
                deletePeerRow(peerId);
                var peerKeys = Object.keys(other_peers);
                if (peerKeys.length == 0) {
                 //   peerView.innerHTML = "No available peers.";
                }
            }

        }

        function deletePeerRow(peerId) {
            var peerElem = document.getElementById("peer" + peerId);
            var table = document.getElementById("peersLog");

            var index = peerElem.rowIndex;
            table.deleteRow(index);
        }

        function onRemoteHangup() {
            trace("onRemoteHangup");
            trace('Session terminated.');

            started = false;
            activeCall = false;
            g_peer_id = -1;
		 
            endCall();
            trace("onRemoteHangup-");
        }

        function handleServerNotification(rawdata) {
            trace("[handleServerNotification]: " + rawdata);
		     /*var jsondata = JSON.parse(rawdata);
		     var data = jsondata[0];
		     trace("data: " + data);
            var parsed = data.split(',');*/
			var name = rawdata.name;
            var peerId = rawdata.peerId;
            var connectflag = rawdata.flag;
            trace("[handleServerNotification] peerId= " + peerId);
            
            if (  connectflag == 0 )
            {
            	onRemoteHangup();
            }

            if (connectflag != 0) {
                updatePeers(true, peerId, name);
				//stopPreviewVideo();
            } else {
                if (other_peers[peerId] != "undefined") {
                    updatePeers(false, peerId, "");
                }
            }

           // trace("[handleServerNotification] number of peers " + Object.keys(other_peers).length);
           // trace("[handleServerNotification] otherPeers " + parseInt(parsed[1]) + "=" + parsed[0]);
        }

        function hangingGetCallback(socket, hangingGet) {
            //trace("[hangingGetCallback]");
			//logger(" --response---hangingGetCallback " ,"","",hangingGet.message_type);
			var progma=-2;
            try {
                
                
               if (!hangingGet) {
		            if( my_id != -1 ) {}           
                       //disconnect();
                } else {
		            //var progma = GetIntHeader(hangingGet, "Pragma");
					if(hangingGet.message_type==='answer'||hangingGet.message_type==='ice_candidate')
					{
						//logger(" hangingGetCallback inside if (offer,answer,candidate)" ,"","",hangingGet.message_type);
						for (var i=1; i<=Object.keys(other_peers).length; i++) {
						
							if( other_peers[i].med_id === hangingGet.media_session_id){
								progma= i;
								break;
							}
						}
						connectedPeerId = progma;
                        connectedPeerName = other_peers[progma].name;
						
					}
					else if (hangingGet.message_type==='offer')
					{
						for (var i=1; i<=Object.keys(other_peers).length; i++) {
						
							if( other_peers[i].name === hangingGet.peer_display_name){
								progma= i;
								if(!other_peers[i].med_id)
								{
									var obj_mod = {
											name : hangingGet.peer_display_name,
											call_id : hangingGet.call_id,
											conf_id : hangingGet.conference_id,
											med_id : hangingGet.media_session_id,
											txn_id : hangingGet.transaction_id
										}
									other_peers[i]= obj_mod;
								}
								break;
							}
						}
						connectedPeerId = progma;
                        connectedPeerName = other_peers[progma].name;
					}
					//peer_id = progma;
		            if(progma != '-1') // getIntHeaer���� ���� ���� �� -1 �� ��
		            	peer_id = progma;
		           // console.log('from ajax.peer_id', peer_id);
		            console.log('progma:', progma,'peer_id:',peer_id);
		            //var resp = hangingGet.response;
	                /*if (hangingGet) {
	                    //trace("hangingGet.abort");
	                    hangingGet.abort();
	                    hangingGet = null;
	                }*/
                  //  if (peer_id == my_id) {
                   //     handleServerNotification(resp);
                   // } else if(peer_id == -1) {
						//ignore
					//} else {
                      //  connectedPeerId = peer_id;
                      //  connectedPeerName = other_peers[peer_id].name;
                        handlePeerMessage(peer_id, hangingGet);
                  //  }
                }


		        if (my_id != -1)
				{
                 //   window.setTimeout(startHangingGet, 500);
				}
            } catch (e) {
                trace_rt("Hanging get error: " + e.description);
            }
        }

        function onHangingGetTimeout() {
            trace("hanging get timeout. issuing again.");
            hangingGet.abort();
            hangingGet = null;
            if (my_id != -1)
            	//startHangingGet();
                window.setTimeout(startHangingGet, 500);
        }
        
        function startHangingGet() {
            //trace("[startHangingGet]");
            try {
                hangingGet = new XMLHttpRequest();
                hangingGet.onreadystatechange = hangingGetCallback;
                hangingGet.ontimeout = onHangingGetTimeout;
                hangingGet.open("GET", server + "/wait/peer_id/" + my_id, true);
                hangingGet.send();
            } catch (e) {
                trace("error" + e.description);
            }
        }

   /*     function signInCallback() {
            trace("[signInCallback]");
            try {
                if (request.readyState == 4) {
                    if (request.status == 200) {
                        var peers = request.responseText.split("\n");
                        var peer = peers[0];
                        if (peer.length > 2) {
	                        peer = peer.substr(1, peer.length - 2);
	                        my_id = parseInt(peer.split(',')[1]);

	                        trace("My id: " + my_id);
	                        trace("peers: " + peers);
                        }

                        for (var i = 1; i < peers.length; ++i) {
                        	peer = peers[i];
                            if (peer.length > 2) {
		                        peer = peer.substr(1, peer.length - 2);
                                trace("Peer " + i + ": " + peer);
                                g_peer_id = i;
                                var parsed = peer.split(',');
                                updatePeers(true, parseInt(parsed[1]), parsed[0]);
                            }
                        }

/*                        
Peer 1: "my_name,1,1"



                        startHangingGet();
                        request = null;
                        signed_In = true;
                        showSignInStatus();
                        started = true;
                    }
                }
            } catch (e) {
                trace("error: " + e.description);
            }
        }
		
*/		
		function signInCallback1(response) {
            trace("[signInCallback]");
			peerid_others=1;
			for(var i=0; i<response.MeetingInfo.Attendees.length; i++) {
				g_peerid = i+1;
				if(response.MeetingInfo.Attendees[i].User.DisplayName != $("#username").val()) {
				updatePeers(true, peerid_others, response.MeetingInfo.Attendees[i].User.DisplayName);
				peerid_others += 1;
				}
				else {
				my_id = 0;
				}
			}
			signed_In = true;
			//$(document).trigger("showSignInStatusEvent");
			showSignInStatus();
			started = true;
            //
			//$(document).trigger("showSignInStatusEvent");
            //started = true;         
			
			/*
            try {
                if (request.readyState == 4) {
                    if (request.status == 200) {
                        var peers = request.responseText.split("\n");
                        var peer = peers[0];
                        if (peer.length > 2) {
	                        peer = peer.substr(1, peer.length - 2);
	                        my_id = parseInt(peer.split(',')[1]);

	                        trace("My id: " + my_id);
	                        trace("peers: " + peers);
                        }

                        for (var i = 1; i < peers.length; ++i) {
                        	peer = peers[i];
                            if (peer.length > 2) {
		                        peer = peer.substr(1, peer.length - 2);
                                trace("Peer " + i + ": " + peer);
                                g_peer_id = i;
                                var parsed = peer.split(',');
                                updatePeers(true, parseInt(parsed[1]), parsed[0]);
                            }
                        }

/*                        
Peer 1: "my_name,1,1"
*/
/*

                        startHangingGet();
                        request = null;
                        signed_In = true;
                        showSignInStatus();
                        started = true;
                    }
                }
            } catch (e) {
                trace("error: " + e.description);
            }*/
        }
		

        function showSignInStatus() {
           // var avPeers = document.getElementById("availablePeers");
          //  var peerData = document.getElementById("idpeersData");
            trace("showPeers signed_In=" + signed_In);
           if (signed_In) {
                //avPeers.innerHTML = "Available peers";
             //   peerData.style["visibility"] = "visible";
               // trace("zxxxxxxxxx" + signed_In);
                document.getElementById("start").disabled = true;
               document.getElementById("disconnect").disabled = false;
                // trace("sxxxxxxxxx" + signed_In);
            } else {
              //  avPeers.innerHTML = "";
              //  peerData.style["visibility"] = "hidden";
                document.getElementById("start").disabled = false;
                document.getElementById("disconnect").disabled = true;
            }
        }

        function signIn() {
            trace("[signIn]");
			/*
            try {
                request = new XMLHttpRequest();
                request.onreadystatechange = signInCallback;
                request.open("GET", server + "/sign_in/" + localName, true);
                request.send();
            } catch (e) {
                trace("error: " + e.description);
            } */
			
			// send websocket login event
			var username = $("#username").val();
			var email = $("#email_id").val();
			var roomId = $("#room_no").val();
			var server = $("#server").val();
			$(document).trigger("loginEvent", [username, email, roomId]);
			
        }

        function getpermission() {
          //  location.href = document.getElementById("server").value.toLowerCase()+"/list";

        }
        function connect() {
			//document.getElementById("para").innerHTML="sign in clicked";
            trace("[connect]");
			
			//startPreviewVideo();
			
            var str = document.getElementById("username").value;
            if (str) {
                if (onlyASCII)
                    var test = str;
                else
                    var test = encode_utf8(str);

                localName = test;
              //  server = document.getElementById("server").value.toLowerCase();
                if (localName.length == 0) {
                    alert("I need a name please.");
                    document.getElementById("username").focus();
                } else {
                    signIn();
					//logger(" --request--signed in" ,"","","");
                }
            } else {
                alert("Please enter you name");
                document.getElementById("username").focus();
            }
			
        }

        function outgoingCall(peer_id) {
            trace("[outgoingCall] SEND OUTGOINGCALL FOR INITIALIZATION WEBRTC to peer: " + peer_id);
			//logger(" out going call called" ,"","","");
            peer_id *= 1;
            hcap.webrtc.outgoingCall({
                "localId": my_id,
                "remoteId": peer_id,
                "onSuccess": function() {
                    trace("onSuccess to call outgoingCall");
                    started = true;
                    g_peer_id = peer_id; //������ ���� ����
                    //document.getElementById("peer_id").disabled = true;
					//logger(" out going call success" ,"","","");
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                    g_peer_id = -1;
                    started = false;
					//logger(" out going call failed" ,"","","error  :  "+f.errorMessage);
                    //document.getElementById("peer_id").disabled = false;
                }
            });
        }

        function receiveMsgHandler(param) {
            trace("[receiveMsgHandler]");
            trace("Event 'webrtc_message_received' is received.\n" +
                "remotePeerId = " + param.remotePeerId + "\n" +
                "message = " + param.message);
            //sendMessage(param.remotePeerId, param.message);^M                                        

            sendMessage(param.remotePeerId, param.message);
            //sendMessage(param.remotePeerId, AsciiHex2Str(param.message));
        }

        function init() {
            trace("[init]");
            init_key_listener();

          //  document.getElementById("peersLog").innerHTML = "No available peers.";
          //  document.getElementById("peersLog").style["left"] = "0px";
          //  document.getElementById('local').onkeypress = function(e) {
           //     if (e.keyCode == 13) {
           //         document.getElementById('start').click();
           //     }
          //  }

            document.addEventListener(
                "webrtc_message_received",
                function (param) {
                    // {Number} param.remotePeerId - remote peer id
                    // {String} param.message - message
                    trace("Event 'webrtc_message_received' is received.");
                    trace("remotePeerId = " + param.remotePeerId + ", message = " + param.message);

                    //sendMessage(param.remotePeerId, param.message);^M                                        
                    
                    sendMessage(param.remotePeerId, param.message);
                    //sendMessage(param.remotePeerId, AsciiHex2Str(param.message));
                },
                false
            );
            
            document.addEventListener(
                "webrtc_event_received",
                function (param) {
                    // {String} param.eventString - WebRTC event string
                    trace("[webrtc_event_received] event = "+ param.eventString + " is received");
                    					
					if(param.eventString == "p2p_av_disconnected"){
						trace("eventString = " + param.eventString);
						activeCall = false;
						isConnected = false;
					} else if(param.eventString == "p2p_av_connected"){
						isConnected = true;
					}
                },
                false
            );
            
			hcap.property.setProperty({
				 "key" : "browser_https_security_level", 
				 "value" : "1", 
				 "onSuccess" : function() {
					 console.log("onSuccess to set property 'browser_https_security_level' as 1");
				 }, 
				 "onFailure" : function(f) {
					 console.log("onFailure to set property: errorMessage = " + f.errorMessage);
				 }
			});
			
            randomString();
        }
        
        window.addEventListener('load', init);
       // window.onbeforeunload = disconnect;