
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) === false) {
  if(localStorage.getItem('validateRequ') === "true" ) {
	  createDB();
  }
 
}

var db;
function createDB() {
	window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	 
	
	 window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
	 window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
	 
	 if (!window.indexedDB) {
		 
		 console.log("Your browser doesn't support a stable version of IndexedDB.")
	 } else {
	
		 var request = window.indexedDB.open("martti", 1);
		 
		 request.onerror = function(event) {
			 console.log("could not create Indexed DB");
		 };
		 
		 request.onsuccess = function(event) {
			 console.log("Indexed DB  created successfully");
		    db = request.result;
		 };
		 
		 request.onupgradeneeded = function(event) { 
		    var db = event.target.result;
		    var objectStore = db.createObjectStore("user", {keyPath: "id"});
		    console.log("user table  created successfully");
		 }
	 
	 }
}

 function getUser(location) {  
	
	 if(typeof db === "undefined") {
		 createDB(); 
	 }
	 try  {  
		var transaction = db.transaction(["user"]);
	    var objectStore = transaction.objectStore("user");
	    var request = objectStore.get("01");
	    
	    request.onerror = function(event) {
	        
	       if(location=="home") {
	    	   window.location = "index.html";
	       }
	       $("#loader").hide();
	    };
	    
	    request.onsuccess = function(event) {
	       // Do something with the request.result!
	    	
	       if(request.result) {
	    	   
	    	  localStorage.setItem('userExists', 'yes'); 
	          app.auth(request.result.uname, request.result.pword);
	       }
	       
	       else {
	    	   localStorage.setItem('userExists', 'no'); 
	    	   
	    	   if(location=="home") {
	        	   window.location = "index.html";
	           }
	    	   $("#loader").hide();
	    	   
	       }
	    };
	 } 
	 catch (e) {  	
		 $("#loader").hide();
		 if(location=="home") {
      	   window.location = "index.html";
         } 
		 
		 
	 }
	    
 }
 
 
 function addUser(username, pwd) {  
	 if(typeof db === "undefined") {
		 createDB(); 
	 }
	 
	 try  {
		
	    var request = db.transaction(["user"], "readwrite")
	    .objectStore("user")
	    .add({ id: "01", uname: username, pword: pwd });
	    
	    request.onsuccess = function(event) {
	    	console.log("successfully added user to DB");
	    	window.location = "home.html";
	    };
	    
	    request.onerror = function(event) {
	    	console.log("could not add USER to Indexed DB");
	    	window.location = "home.html";
	    }
	 } 
	 catch (e) {
		 $("#loader").hide();
		
		 console.log("Could not add USER to Indexed DB");
		 window.location = "home.html";
	 }
	 
 }
 
 function remove() {
    var request = window.indexedDB.deleteDatabase("martti");
    
    request.onsuccess = function(event) {
    	 
    };
	request.onerror = function(event) {
		 
    }
 }