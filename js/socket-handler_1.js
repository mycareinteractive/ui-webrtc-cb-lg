/*globals  io , console , users , peerConnection , $ , count:true, video_src,connection_obj, localStream */
/*global startLocalVideo , createpeerobject , setremoteanswer , seticecandidates , clearImmediate */
/*exported  SocketHandler,emitSdp,emitIce*/
/* jshint proto: true */
"use strict";

// Creation of Transaction ID
var L, iter;
for (L = [], iter = 0; 256 > iter; iter++) {
    L[iter] = (16 > iter ? "0" : "") + iter.toString(16);
}

function get_transaction_id() {
    var a = 4294967295 * Math.random() | 0,
        b = 4294967295 * Math.random() | 0,
        c = 4294967295 * Math.random() | 0,
        d = 4294967295 * Math.random() | 0;
    return L[255 & a] + L[a >> 8 & 255] + L[a >> 16 & 255] + L[a >> 24 & 255] + "-" + L[255 & b] + L[b >> 8 & 255] + "-" + L[b >> 16 & 15 | 64] + L[b >> 24 & 255] + "-" + L[63 & c | 128] + L[c >> 8 & 255] + "-" + L[c >> 16 & 255] + L[c >> 24 & 255] + L[255 & d] + L[d >> 8 & 255] + L[d >> 16 & 255] + L[d >> 24 & 255];
}

var PING_INTERVAL = 60 * 1000;
var PingHandler = null;
var meetingRefGlobal = null;
var userIdGlobal = null;
var socket_g;
var endpointkey;
var publicendpointkey;
var disconnect_updateatendee;
var userInfo = [];
var NumberOfUsers;
var flag1 = false;
//handling websocket and onclick events
var SocketHandler = function(signalingServerURL) {
    if (!this.socket) {
        this.socket = io(signalingServerURL);
		this.socket.on('error', function(exception) {
			//logger(" error " ,"","","error = "+exception);
		})
        this.socket.on("connect", socketConnected);
        this.socket.on("disconnect", socketDisconnected);
        this.socket.on("SocketTestPong", socketPongEvent);
        var socket = this.socket;
       $(document).on("loginEvent", function(event, username, email, room) {
            var obj = {
                username: username,
                email: email,
				room: room
            };
            userInfo.push(obj);
            login(socket);
        });
        this.socket.on("Welcome", welcome.bind(this, this.socket));
        this.socket.on("AddMeeting", addMeeting.bind(this, this.socket));
        this.socket.on("UpdateAttendee", disconnectUpdateAttendee.bind(this, this.socket));
        this.socket.on("UserEnteredRoom", UserEnteredRoom.bind(this, this.socket));
       this.socket.on("PrefetchMeetingResponse", join.bind(this, this.socket));
        this.socket.on("SendInstantMessage", sendInstantMessage.bind(this, this.socket));
        $(document).on("connectEvent", function() {
            join(socket);
        });
		 $(document).on("sdpEvent", function(event, message,peer_id) {
			 if(message){
				// logger(" inside sdpevent" ,"","","");
				emitSdp(socket,message,peer_id );
			 }
        });
        //this.socket.on("WebRTCResponse", handlePeerResponse.bind(this, this.socket));
		this.socket.on("WebRTCResponse", hangingGetCallback.bind(this, this.socket));
        $(document).on("confirmDisconnectEvent", function() {
			//logger(" confirmDisconnectEvent " ,"","","");
            disconnectFunction(socket);
        });
		$(document).on("keep_aliveEvent", function(event, resp) {
            keepAlive(socket,resp);
        });
        $(document).on("SendMsgEvent", function() {
            sendMsg(socket);
        });
       
	  /* $(document).on("logoutEvent", function() {
            logoutFunction(socket);
        });*/
       // this.socket.on("AuthChallenge", pageReload);
        $(document).on("getMessageTextEventResponse", function(event, msg) {
            if (socket) {
                SendTextMessage(socket, msg);
            }
        });
        socket_g = this.socket;
		
    }
};

var socketConnected = function() {
	//logger(" socketConnected " ,"","","");
    startPing(this);
};

var socketDisconnected = function() {
	//logger(" socketDisconnected " ,"","","");
    stopPing(this);
};

var startPing = function(socket) {
    if (!PingHandler) {
        PingHandler = setInterval(function() {
            socket.emit("SocketTestPing", { sentTime: new Date().toISOString() });
        }, PING_INTERVAL);
    }
};

var stopPing = function() {
    clearImmediate(PingHandler);
    PingHandler = null;
};

var socketPongEvent = function() {
    console.debug(arguments);
};

// sending login request
var login = function(socketObject) {
    var check_login_flag = "Normal_login"; //to login normally
    startLogin(socketObject, check_login_flag);
    //startLocalVideo();
};

var startLogin = function(socket, check_login_flag) {
    if (socket && check_login_flag === "Normal_login") {
        socket.emit("Login", {
            Capabilities: ["web", "html5content", "content", "chat"],
            HostPath: "https://meetme.carenection.com/",
            //DisplayName: document.getElementById("username").value,
            DisplayName: $("#username").val(),
            EndpointKey: 'NULL',
            PublicEndpointKey: "null",
            Realm: "anonymous",
            //UserAddress: document.getElementById("email_id").value,
            UserAddress: $("#email_id").val(),
            rid: "rid_2"
        });

    }
    if (socket && check_login_flag === "Logout_login") {
        socket.emit("Login", {
            Capabilities: ["web", "html5content", "content", "chat"],
            HostPath: "https://meetme.carenection.com/",
            EndpointKey: endpointkey,
            PublicEndpointKey: publicendpointkey,
            rid: "rid_2"
        });
		pageReload();
    }
};

// receiving welcome response
var welcome = function(socket, response) {
    var room_val = userInfo[0].room;
    endpointkey = response.PrivateUserSessionInfo.EndpointKey;
    publicendpointkey = response.User.PublicEndpointKey;
    enterLobby(socket, endpointkey, room_val);
};
var sendInstantMessage = function(socket, response) {
        $(document).trigger("gotTextMessageEvent", response);
    };
    // sending enterLobby request
var enterLobby = function(socket, endpointkey, room_val) {
    socket.emit("EnterLobby", {
        LobbyCode: room_val,
        EndpointKey: endpointkey
    });
};

// receiving addMeeting response
var addMeeting = function(socket, response) {
    NumberOfUsers = response.MeetingInfo.Attendees.length;
    userIdGlobal = response.Attendee.User.UserId;
    var userName = response.Attendee.User.DisplayName;
    meetingRefGlobal = response.MeetingInfo.MeetingRef;
    var updateAttendeeInfo = response.Attendee;
    var lobbyCode = response.MeetingInfo.LobbyCode;
	
	//call signincallback()
	signInCallback1(response);
	
	
    //sending ClientStatusUpdate request
    socket.emit("ClientStatusUpdate", {
        "description": "Adding a given modality to the modality list.",
        "userDetails": {
            "id": userIdGlobal,
            "userName": userName + "(Me)",
            "meetingId": meetingRefGlobal
        }
    });
    updateAttendee(socket, updateAttendeeInfo);
    prefetchMeeting(socket, lobbyCode);
};

//sending updateAttendee request
var updateAttendee = function(socket, updateAttendeeInfo) {
    updateAttendeeInfo.Capabilities.push("audiovideo");
    updateAttendeeInfo.Modalities.push("audiovideo");
    socket.emit("UpdateAttendee", updateAttendeeInfo);
};

//sending prefetchMeeting request
var prefetchMeeting = function(socket, lobbyCode) {
    socket.emit("PrefetchMeeting", { LobbyCode: lobbyCode });
};

//sending webrtc join_conference_request
var join = function(socketObject, pre_response) {
    //$(document).trigger("changeDisplayEvent");
    joinContinue(socketObject);
};

//sending join conference request
var joinContinue = function(socket) {
    if (socket) {
        socket.emit("WebRTCRequest", {
            MeetingRef: meetingRefGlobal,
            Content: {
                authorized: false,
                //display_name: document.getElementById("username").value,
                display_name: $('#username').val(),
                message_type: "join_conference_request",
                //passback: document.getElementById("email_id").value,
                passback: $('#email_id').val(),
                protocol_version: "Collabutron v0.1",
                transaction_id: get_transaction_id(),
                user_agent: "Polycom_RPWS_Client/2.1.2.730-229386 (WebRTC; ECS_User) ",
                //vmr_id: document.getElementById("room_no").value
                vmr_id: $('#room_no').val()
            },
            UserId: userIdGlobal
        });
    }
	//logger(" request---join continue " ,"","","");
};

//sending logout request
var logoutFunction = function(socket) {
    if (socket) {
        socket.emit("Logout", {
            Capabilities: ["web", "html5content", "content", "chat"],
            //DisplayName: document.getElementById("username").value,
            DisplayName: $("#username").val(),
            EndpointKey: endpointkey,
            HostPath: "https://meetme.carenection.com/",
            PublicEndpointKey: publicendpointkey,
            Realm: "anonymous",
            //UserAddress: document.getElementById("email_id").value,
            UserAddress: $("#email_id").val(),
            UserId: userIdGlobal,
            //UserName: document.getElementById("username").value,
            UserName: $("#username").val(),
            rid: "rid_1"
        });
        var check_login_flag = "Logout_login";
        //userInfo = null;
        startLogin(socket, check_login_flag);
    }
};

//receiving and handling different webrtc Responses
var handlePeerResponse = function(socket, response) {
    if (response.message_type === "solicit_offer") {
        createpeerobject(socket, response);
    }
    if (response.message_type === "answer") {
        var objsdpanswer = {
            type: response.message_type,
            sdp: response.sdp,
            __proto__: response.__proto__
        };
        setremoteanswer(socket, response, objsdpanswer);

    }
    if (response.message_type === "ice_candidate" && response.ice_candidate !== null) {
        var objice = {
            candidate: response.ice_candidate.candidate,
            sdpMLineIndex: response.ice_candidate.candidate.sdpMLineIndex,
            sdpMid: response.ice_candidate.candidate.sdpMid
        };
        seticecandidates(socket, response, objice);
    }
    if (response.message_type === "keep_alive") {
        socket.emit("WebRTCRequest", {
            MeetingRef: meetingRefGlobal,
            Content: {
                message_type: "keep_alive_response",
                transaction_id: response.transaction_id,
                call_id: response.call_id
            },
            UserId: userIdGlobal
        });
    }
    if (response.message_type === "offer") {
        var peerName = response.peer_display_name;
        var flag = 1; // user joined
        $(document).trigger("notifyEvent", [peerName, flag]);
        createpeerobject(socket, response);
    }
};
//sending webrtc sdp
var emitSdp = function(socket, localDescription, response_id) {
	var localDescription = JSON.parse(localDescription);
	//logger(" sending sdpevent in emitsdp" ,"","response_id = "+response_id,"localDescription = "+localDescription);
    if (localDescription.type === "offer") { //sending sdp offer
		//logger(" sending offer" ,"","","");
        socket.emit("WebRTCRequest", {
            MeetingRef: meetingRefGlobal,
            Content: {
                conference_id: other_peers[response_id].conf_id,
                media_session_id: other_peers[response_id].med_id,
                call_id: other_peers[response_id].call_id,
                message_type: "offer",
                sdp: localDescription.sdp,
                transaction_id: other_peers[response_id].txn_id
            },
            UserId: userIdGlobal
        });
	//logger(" request---sending sdpevent offer" ,"","","sdp = "+localDescription);	
    }
    if (localDescription.type === "answer") { //sending sdp answer
		//logger(" sending answer" ,"","","");
        socket.emit("WebRTCRequest", {
            MeetingRef: meetingRefGlobal,
            Content: {
                conference_id: other_peers[response_id].conf_id,
                media_session_id: other_peers[response_id].med_id,
                call_id: other_peers[response_id].call_id,
                message_type: "answer",
                sdp: localDescription.sdp,
                transaction_id: other_peers[response_id].txn_id
            },
            UserId: userIdGlobal
        });
		
	//logger(" request---sending sdpevent answer" ,"","","sdp = "+localDescription);	
    }
	if (localDescription.type === "candidate") {
	//	logger(" sending candidate" ,"","","");
        socket.emit("WebRTCRequest", {
            MeetingRef: meetingRefGlobal,
            Content: {
                conference_id: other_peers[response_id].conf_id,
                media_session_id: other_peers[response_id].med_id,
                call_id: other_peers[response_id].call_id,
                message_type: "ice_candidate",
                ice_candidate: localDescription,
                transaction_id: other_peers[response_id].txn_id
            },
            UserId: userIdGlobal
        });
	//logger(" request---sending sdpevent candidate" ,"","","candidate = "+localDescription);	
	};
}
//sending webrtc ICE candidates
/*var emitIce = function(socket,response, icandidate) {
    if (icandidate) {
        socket_g.emit("WebRTCRequest", {
            MeetingRef: meetingRefGlobal,
            Content: {
                conference_id: response.conference_id,
                media_session_id: response.media_session_id,
                call_id: response.call_id,
                message_type: "ice_candidate",
                ice_candidate: icandidate.candidate,
                transaction_id: response.transaction_id
            },
            UserId: userIdGlobal
        });
    }
};*/

var UserEnteredRoom = function(socket, response) {
	var progma;
	var name;
	var flag;
	
	//other_peers[peerid_others] = response.Attendee.User.DisplayName;
	/*for (var i=1; i<=Object.keys(other_peers).length; i++) {
						if( other_peers[i] === response.User.DisplayName){
						progma = i;
						name = response.User.DisplayName;
						flag = 1;
						break;
						}
		}*/
		flag = 1;
		name = response.Attendee.User.DisplayName;
	var object = {
			peerId : peerid_others,
			name : name,
			flag : flag
		}
	handleServerNotification(object);
	peerid_others += 1;
}
var disconnectUpdateAttendee = function(socket, response) {
	
    var progma;
	var name;
	var flag;
    if (response.ConnectionState === "disconnected") { //closing remote peerconnection
        //$(document).trigger("userLeftEvent", [response.User.DisplayName]);
		for (var i=1; i<=Object.keys(other_peers).length; i++) {
						if( other_peers[i] === response.User.DisplayName){
						progma = i;
						name = response.User.DisplayName;
						flag = 0;
						break;
						}
						var object = {
										peerId : progma,
										name : name,
										flag : flag
									}
						handleServerNotification(object);
		}
		
    }
	//var name = $("#username").val();
    if (response.MuteStatus && response.ExternalParticipant.ExternalDisplayName === $("#username").val()) {
        disconnect_updateatendee = response;
        disconnect_updateatendee.state = "disconnected";
		if (response.ConnectionState === "disconnected") {
			endCall();
			logoutFunction(socket);
		}
    }
    
};

//disconnecting local user
var disconnectFunction = function(socket) {
    /*for (var i = 0; i < users.length; i++) {
        peerConnection[users[i].peer_id].close(); //closing all local peerconnection
    }*/
    socket.emit("ClientStatusUpdate", JSON.stringify({
        "description": "Dropping self from the meeting.",
        "userDetails": {
            "id": userIdGlobal,
            "userName": $("#username").val() + "(Me)",
            "meetingId": meetingRefGlobal
        }
    }));
    socket.emit("UpdateAttendee", disconnect_updateatendee);
	//logoutFunction(socket);
    //$(document).trigger("disconnectFunctionEvent");
};


//reload page after logout
var pageReload = function() {
	//logger(" --page reload " ,"","","");
    cleanGlobalVar();
    console.log("reload");
    window.location.reload();
};

// all global variables reset to null.
function cleanGlobalVar() {
    userIdGlobal = null;
    meetingRefGlobal = null;
    socket_g = null;
    endpointkey = null;
    publicendpointkey = null;
    disconnect_updateatendee = null;
    userInfo = null;
   // video_src = null;
   // peerConnection = null;
    //users = null;
    //connection_obj = null;
    //count = null;
    //localStream = null;
	other_peers = null;
}


var keepAlive = function(socket,resp) {
	//logger(" request---keepAlive sent" ,"","","");
   socket.emit("WebRTCRequest", {
            MeetingRef: meetingRefGlobal,
            Content: {
                message_type: "keep_alive_response",
                transaction_id: resp.transaction_id,
                call_id: resp.call_id
            },
            UserId: userIdGlobal
        });
		
};

//sending chat message
var sendMsg = function () {
    if (event.keyCode == 13) {
        $(document).trigger("getMessageTextEvent");
    }
}
/*
function SendTextMessage(socket, msg) {
    socket.emit("SendInstantMessage", {
        MeetingRef: meetingRefGlobal,
        ContentType: "text/plain",
        UserDisplayName: userInfo[0].username,
        UserId: userIdGlobal,
        Content: msg
    });

}*/
