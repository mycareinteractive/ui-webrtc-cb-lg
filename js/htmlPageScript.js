




$(function () {
var
scroll,
scrollTimeout;
keyAttr=3;
instPwr="1";

$('body').on('onload', function (event) {
	event.preventDefault();
		
	$('.pre-console').html(
	'<b>deltaX:</b>' + event.deltaX + '<br>' +
	'<b>deltaY:</b>' + event.deltaY + '<br>' +
	'<b>deltaFactor:</b>' + event.deltaFactor +
	'</p>');
	
});

// if you change the test case, apply the new test case.
$('#btnPermission').on('click', function (event) {
	getpermission();
});

$('#btnGenName').on('click', function (event) {
	randomString();
});

/*$('#connect').on('click', function (event) {
	connect();
});*/


/*$('#disconnect').on('click', function (event) {
	disconnect();
});*/


$('#startPreviewVideo').on('click', function (event) {
	startPreviewVideo();
});


$('#getConfiguration').on('click', function (event) {
	getConfiguration();
});


$('#setConfiguration1').on('click', function (event) {
	setConfigurationTurn();
});


$('#setConfiguration2').on('click', function (event) {
	setConfigurationAudioConf();
});


$('#btnRefresh').on('click', function (event) {
	event.preventDefault();
	window.location.reload();
});

$('#lbAttr0').on('click', function (event) {
	event.preventDefault();
	keyAttr=0;

	hcap.mode.setHcapMode({
	     "mode" : hcap.mode.HCAP_MODE_0,
	     "onSuccess" : function() {
	         console.log("onSuccess");
	     }, 
	     "onFailure" : function(f) {
	         console.log("onFailure : errorMessage = " + f.errorMessage);
	     }
	});	
	
	$('.pre-console').html(
		'<b>hacp mode is selected with '+ keyAttr + ' </b><br>' +	
		'</p>');
});

$('#lbAttr1').on('click', function (event) {
	event.preventDefault();
	keyAttr=1;

	hcap.mode.setHcapMode({
	     "mode" : hcap.mode.HCAP_MODE_1,
	     "onSuccess" : function() {
	         console.log("onSuccess");
	     }, 
	     "onFailure" : function(f) {
	         console.log("onFailure : errorMessage = " + f.errorMessage);
	     }
	});	
	
	$('.pre-console').html(
		'<b>hacp mode is selected with '+ keyAttr + ' </b><br>' +	
		'</p>');	
	});
		
		
$('#lbAttr2').on('click', function (event) {
	event.preventDefault();
	keyAttr=2;

	hcap.mode.setHcapMode({
	     "mode" : hcap.mode.HCAP_MODE_2,
	     "onSuccess" : function() {
	         console.log("onSuccess");
	     }, 
	     "onFailure" : function(f) {
	         console.log("onFailure : errorMessage = " + f.errorMessage);
	     }
	});		
	$('.pre-console').html(
		'<b>hacp mode is selected with '+ keyAttr + ' </b><br>' +	
		'</p>');
});

$('#lbAttr3').on('click', function (event) {
	event.preventDefault();
	keyAttr=3;

	hcap.mode.setHcapMode({
	     "mode" : hcap.mode.HCAP_MODE_3,
	     "onSuccess" : function() {
	         console.log("onSuccess");
	     }, 
	     "onFailure" : function(f) {
	         console.log("onFailure : errorMessage = " + f.errorMessage);
	     }
	});	
		
	$('.pre-console').html(
		'<b>hacp mode is selected with '+ keyAttr + ' </b><br>' +	
		'</p>');
});


$('#btnClear').on('click', function (event) {
//	event.preventDefault();
	startPreviewVideo();
	
	$('.pre-console').html(
		'<b>Preview Start </b><br>' +	
		'</p>');	
	
});

$('#btnSonifi').on('click', function (event) {
	event.preventDefault();	
    var ret;
            hcap.webrtc.getConfiguration({
                "onSuccess": function(s) {
                    trace("onSuccess : configurations = " + s.configurations);
                    ret = s.configurations;
                },
                "onFailure": function(f) {
                    trace("onFailure : errorMessage = " + f.errorMessage);
                }
            });

	$('.pre-console').html(
		'<b>Get Configuration :' + ret +	 '</b><br>' +
		'</p>');		
});	
	
	
$('#btnSonifi-Add').on('click', function (event) {
setConfigurationTurn();
	$('.pre-console').html(
		'<b>Set Turn Configuration </b><br>' +	
		'</p>');
});

$('#btnSonifi-TestSuite1').on('click', function (event) {
setConfigurationAudioConf();
	$('.pre-console').html(
		'<b>Set Audio Configuration </b><br>' +	
		'</p>');
});

$('#btnGetHcapMode').on('click', function (event) {
	var ret;
	hcap.mode.getHcapMode({
	     "onSuccess" : function(s) {
	         console.log("onSuccess : current hcap mode = " + s.mode);
	         ret = s.mode;
	     }, 
	     "onFailure" : function(f) {
	         console.log("onFailure : errorMessage = " + f.errorMessage);
	     }
	});
	$('.pre-console').html(
		'<b>HCAP MODE :' + ret +' </b><br>' +	
		'</p>');
});



/* register event listener */
document.addEventListener("channel_changed", function(param) {
	//$('#Start').button('loading'); 
	$('.pre-console').html(
		'<b>receive channel changed event : </b><br>' +			
		'</p>');
}, false);

hcap.property.setProperty({
	"key":"tv_channel_attribute_floating_ui",
	"value":"0",
	"onSuccess":function() {
		console.log("hcap.property.setProperty(tv_channel_attribute_floating_ui) success");
	},
	"onFailure":function(param) {
		console.log("hcap.property.setProperty(tv_channel_attribute_floating_ui) failed");
	}
});

hcap.property.setProperty({
	"key":"tv_channel_ui",
	"value":"0",
	"onSuccess":function() {
		console.log("hcap.property.setProperty(tv_channel_ui) success");
	},
	"onFailure":function(param) {
		console.log("hcap.property.setProperty(tv_channel_ui) failed");
	}
});

var keyReceiveCount = 0;
var HCAP_KEY_TABLE = new Array();


function getIntegerHex(val,start,end) {
	var base16 = val.toString(16);
	var quadString = '';
	for(var x=start; x<end; x++)
		quadString += (!base16[x] || base16[x] == '' ? '0' : base16[x]);
	return quadString;
}


document.onkeydown = function(e) {
	var KeyCode = e.keyCode ? e.keyCode : e.which;
	keyReceiveCount++;
	
	$('.pre-console').html(
		'<b>' + keyReceiveCount + 'th received key code : </b>' + KeyCode + '(0x' + KeyCode.toString(16) + ')' + ', key name = ' + HCAP_KEY_TABLE[KeyCode] + '<br>' +	
		'</p>');	

	processKeyEvent(KeyCode);
	console.log( keyReceiveCount + 'th received key code : ' + KeyCode + '(0x' + KeyCode.toString(16) + ')' + ', key name = ' + HCAP_KEY_TABLE[KeyCode]);
}

var flip = true;
	
function modeChangefromPortal()
{

	
	if ( flip == true )
	{
	
		hcap.mode.setHcapMode({
		     "mode" : hcap.mode.HCAP_MODE_0,
		     "onSuccess" : function() {
		         console.log("onSuccess");
		     }, 
		     "onFailure" : function(f) {
		         console.log("onFailure : errorMessage = " + f.errorMessage);
		     }
		});
		
		flip = false;			
	}	
	else
	{
		hcap.mode.setHcapMode({
		     "mode" : hcap.mode.HCAP_MODE_1,
		     "onSuccess" : function() {
		         console.log("onSuccess");
		     }, 
		     "onFailure" : function(f) {
		         console.log("onFailure : errorMessage = " + f.errorMessage);
		     }
		});	
		flip = true;
	
	}



}
    var curTabIndex = 0;
    function moveFocus(index) {
        var buttons = document.querySelectorAll("button");
        
        lastTabIndex = buttons.length-1;
        console.log('lastTabIndex=',lastTabIndex,'currentTabIndex=',curTabIndex, 'button number=',buttons.length);
        for (var i = 0; i < buttons.length; i++) {
                   console.log('button tabIndex=', buttons[i].tabIndex, 'index=', index);
            if (buttons[i].tabIndex == index) {

                buttons[i].focus();
                curTabIndex = i;
                break;
            }
        }
    }

    $(document).ready(function(){
        $("#connect").focus();
        curTabIndex = $("#connect").attr("tabindex");
        console.log('first index=',curTabIndex);
        moveFocus(curTabIndex);
    });
function processKeyEvent(keyCode)
{
	switch (keyCode)
	{
		case hcap.key.Code.PORTAL:
			//window.location.reload();
			modeChangefromPortal();
			break;
		case hcap.key.Code.NUM_0:			
			break;
		case hcap.key.Code.NUM_1:			
			break;
		case hcap.key.Code.NUM_2:			
			break;
		case hcap.key.Code.NUM_3:			
			break;
		case hcap.key.Code.NUM_4:			
			break;
		case hcap.key.Code.NUM_5:			
			break;
		case hcap.key.Code.NUM_6:			
			break;
		case hcap.key.Code.NUM_7:			
			break;
		case hcap.key.Code.NUM_8:			
			break;
		case hcap.key.Code.NUM_9:			
			break;
		case hcap.key.Code.ENTER:			
			break;
		default:
			break;
	}
    if (keyCode === 39) { // right
        console.log('lastTabIndex=',lastTabIndex);
        var next_index= (curTabIndex < lastTabIndex)?curTabIndex + 1:0;
        if (signed_In == true )
        {
        	if (next_index == 2 )
        	{
        		console.log("sign_in status so skip and add +1");
        		next_index++;
        	}
        }else {
        	if ( next_index == 3 )
        	{
        		console.log("sign_out status so skip and add +1");
        		next_index =  (next_index < lastTabIndex)?next_index + 1:0;
        	}
        }
        moveFocus(next_index);
    } else if (event.keyCode === 37) { // left
       var next_index= (curTabIndex > 0)?curTabIndex - 1:lastTabIndex;
        if (signed_In == true )
        {
        	if (next_index == 2 )
        	{
        		console.log("sign_in status so skip and add +1");
        		next_index--;
        	}
        }else {
        	if ( next_index == 3 )
        	{
        		console.log("sign_out status so skip and add +1");
        		next_index--;
        	}
        }    
        moveFocus(next_index);
    }			
}

/* main start */
hcap.video.setVideoSize({
	"x":850,
	"y":50,
	"width":428,
	"height":270,
	"onSuccess":function(s) {		
		$('.pre-console').html(
			'<b>hcap.video.setVideoSize() success. rect :</b> (850,50,428,270) <br>' +
			'</p>');			
	},
	"onFailure":function(f) {		
		$('.pre-console').html(
			'<b>hcap.video.setVideoSize() faild</b>' + f.errorMessage + '<br>' +			
			'</p>');			
	}
});	


});