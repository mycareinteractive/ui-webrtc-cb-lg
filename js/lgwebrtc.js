
navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.GetUserMedia || navigator.msGetUserMedia;
        windowURL = window.URL || window.webkitURL || window.mozURL || window.msURL;
		var LG_martti={
			isConnected:false,
			myId:-1,
			peerIdOthers:0,
			otherPeers:[],
			callStatus:null, 
			signedIn:false,
			onlyASCII:true,
			iceFlag:0,
			userName:"",
			email:"",
			roomid:"",
			socketStatus:false,
			pingInterval:60 * 1000,
			pingHandler:null,
			meetingRefGlobal:null,
			userIdGlobal:null,
			socketGlobal:null,
			endPointKey:null,
			publicEndPointKey:null,
			disconnectUpdateAttendee:null,
			userInfo:[],
			numberOfUsers:null,
			otherPeersCount:1
		}
		function socketCreate(){
			var initObj={};
			initObj.url="https://meetme.carenection.com";
			initializeSocket(initObj);
		}
		
		
		function initializeSocket(initObj){
		var serverurl=initObj.url;
		"use strict";
		var mySocketHandler = new SocketHandler(serverurl);
		}

		/*endcall ends the webrtc call*/	
        function endCall() {
            hcap.webrtc.endCall({
                "onSuccess": function() {
					logWriter("endCall","success");
                },
                "onFailure": function(f) {
					logWriter("endCall","failed");
                }
            });
            $('.back-button').click();
        }

        function setConfigurationTurn() {
            hcap.webrtc.setConfiguration({
                "key": lgEnviron.keyStr,
                "value": lgEnviron.valueStr,
                "onSuccess": function() {
                logWriter("setConfigurationTurn","success");
                },
                "onFailure": function(f) {
                logWriter("setConfigurationTurn","failed");
                }
            });
        }
       

        function keydown(e) {
            if (!e) e = event;

            var code = (e.keyCode ? e.keyCode : e.which);

            switch (code) {
                case hcap.key.Code.UP:
                case hcap.key.Code.DOWN:
                case hcap.key.Code.ENTER:
                case hcap.key.Code.PORTAL:
                case hcap.key.Code.EXIT:
            }
        }

        function init_key_listener() {
            if (document.addEventListener) {
                document.addEventListener("keydown", keydown, false);
            } else if (document.attachEvent) {
                document.attachEvent("onkeydown", keydown);
            }
        }

        function sendMessage(peer_id, message) {
			if(message){
				$(document).trigger("sdpEvent",[message,peer_id]);
			}
        }
		
		/*api called to send sdp answer to hcap*/
        function doAnswer(peer_id, sdp_msg) {            
            hcap.webrtc.incomingCall({
                "localId": LG_martti.myId,
                "remoteId": peer_id,
                "remoteSDP": sdp_msg,
                "onSuccess": function() {
                    logWriter("incomingCall","success");
                },
                "onFailure": function(f) {
                   logWriter("incomingCall","failed");
                }
            });

            LG_martti.callStatus = "started";
        }

		/*incoming socket messages (solicit offer,offer,answer,ice candidate) are processed in this function*/
        function processSignalingMessage(message, peer_id) {
            try {
            	if (Object.keys(message).length <= 0) {
            		return;
            	}
				var msg = message;
				var foundflag = 0;
				
				if (msg.message_type === 'offer') {
					var sdp_obj={ 
						'type':msg.message_type,
						'sdp' :msg.sdp					
					}
					var msg_encoded = JSON.stringify(sdp_obj);
				    doAnswer(peer_id, msg_encoded);
				    foundflag =1;
				    } else if (msg.message_type === 'answer' && LG_martti.callStatus == "started") {
						var sdp_obj={ 
							'type':msg.message_type,
							'sdp' :msg.sdp					
						}
						var msg_encoded = JSON.stringify(sdp_obj);
	                    hcap.webrtc.acceptMessage({
	                        "remoteId": peer_id,
	                        "rtcMsg": msg_encoded,
	                        "onSuccess": function() {
                            logWriter("acceptMessage","success");
	                        },
	                        "onFailure": function(f) {
                            logWriter("acceptMessage","failed");
	                        }
	                    });
				        foundflag = 1;
				    }  else if (msg.message_type === 'end_media_session' && LG_martti.callStatus == "started") { 
							LG_martti.isConnected = false;
							onRemoteHangup();
							foundflag = 1;
				    } 
					if(msg.message_type === 'solicit_offer') {
						/*for webrtc call*/
						if(msg.mix_type==="mesh")
						{
							for(i = 1; i <=(LG_martti.otherPeers).length; i++) {
								if(msg.peer_display_name === LG_martti.otherPeers[i].name) {
									var obj_mod = {
										peer : LG_martti.otherPeers[i].peer,
										name : msg.peer_display_name,
										call_id : msg.call_id,
										conf_id : msg.conference_id,
										med_id : msg.media_session_id,
										txn_id : msg.transaction_id
									}
								LG_martti.otherPeers[i]= obj_mod;
								outgoingCall(LG_martti.otherPeers[i].peer);
								break;
								}
							}
						}
						/*for sip call*/
						else if(msg.mix_type==="transcoded_mcu")
						{
							LG_martti.iceFlag=1;
							var tempName=LG_martti.otherPeers[LG_martti.otherPeersCount-1].name;
							var obj_mod = {
								peer : LG_martti.otherPeers[LG_martti.otherPeersCount-1].peer,
								name : tempName,
								call_id : msg.call_id,
								conf_id : msg.conference_id,
								med_id : msg.media_session_id,
								txn_id : msg.transaction_id
							}
							LG_martti.otherPeers[LG_martti.otherPeersCount-1]= obj_mod;
							outgoingCall(LG_martti.otherPeers[LG_martti.otherPeersCount-1].peer);
						}
					}	
					if (msg.message_type === "keep_alive") {	
						$(document).trigger("keep_aliveEvent",[msg]);
					}
    
		            if (msg == null) {
		                return;
		            }
					/*for webrtc call*/
		            if (LG_martti.iceFlag===0){	
						if(msg.message_type === 'ice_candidate' && LG_martti.callStatus == "started" && msg.ice_candidate!=null ) {				
							var candidate_obj={ 
								"candidate" :msg.ice_candidate.candidate ,
								"sdpMLineIndex" :msg.ice_candidate.sdpMLineIndex ,
								"sdpMid" : msg.ice_candidate.sdpMid,
								"type" : "candidate"			
							}
							var msg_encoded = JSON.stringify(candidate_obj);
							hcap.webrtc.acceptMessage({
								"remoteId": peer_id,
								"rtcMsg": msg_encoded,
								"onSuccess": function() {
                                logWriter("acceptMessage","success");
									},
								"onFailure": function(f) {
                                logWriter("acceptMessage","failed");
									}	
							});
		                } 
					}
					/*for sip call*/
					else{
						if(msg.message_type === 'ice_candidate'){
							candidate_obj1={ 
								 "type" : "candidate"
							}	
							var msg_enc = JSON.stringify(candidate_obj1);
							hcap.webrtc.acceptMessage({
								"remoteId": peer_id,
								"rtcMsg": msg_enc,
								"onSuccess": function() {
                                logWriter("acceptMessage","success");
										},
								"onFailure": function(f) {
                                logWriter("acceptMessage","failed");
										}
							});
							LG_martti.iceFlag=0;
						}
					}
					
            } catch (e) {
				logWriter(e,"error");
            }
        }

        function handlePeerMessage(peer_id, data) {
            processSignalingMessage(data, peer_id);
        }

		/*deleting the peerid when remote user disconnects*/
        function clearOtherPeers() {
            var peerKeys = (LG_martti.otherPeers);
            for (i = 0; i < peerKeys.length; i++) {
                updatePeers(false, peerKeys[i], "");
            }
        }
		
		/*disconnect function*/
        function disconnect() {
            endCall();
			$(document).trigger("confirmDisconnectEvent");
            LG_martti.signedIn = false;
            //clearOtherPeers();
        }


		/*for creating and deleting remote peer objects accordingly*/
        function updatePeers(add, peerId, name) {
            if (add) {
                var obj = {
					peer: peerId,
					name : name,
					call_id : "",
					conf_id : "",
					med_id : "",
					txn_id : ""
				}
				LG_martti.otherPeers[LG_martti.otherPeersCount]= obj;
				LG_martti.otherPeersCount += 1;
            } else {
                delete LG_martti.otherPeers[peerId];
				   }

        }

        function onRemoteHangup() {
            LG_martti.callStatus = "ended";
            endCall();
        }

		
        function handleServerNotification(rawdata) {  
			var name = rawdata.name;
            var peerId = rawdata.peerId;
            var connectflag = rawdata.flag;
            
            if (  connectflag == 0 ){
            	onRemoteHangup();
            }

            if (connectflag != 0) {
                updatePeers(true, peerId, name);
            } else {
                if (LG_martti.otherPeers[peerId] != "undefined") {
                    updatePeers(false, peerId, "");
                }
            }
        }

		/*this function is called whenever any websocket message arrives from the server*/
        function hangingGetCallback(socket, hangingGet) {
			var tempPeerId=-2;
            try { 
               if (!hangingGet) {
		            if( LG_martti.myId != -1 ) {}           
                } else {
					if(hangingGet.message_type==='answer'||hangingGet.message_type==='ice_candidate'){
						for (var i=1; i<=(LG_martti.otherPeers).length; i++) {
							/*comparing the media session ids to get the peerid of the remote user*/
							if( LG_martti.otherPeers[i].med_id === hangingGet.media_session_id){
								tempPeerId= i;
								break;
							}
						}
					}
					else if (hangingGet.message_type==='offer'){
						for (var i=1; i<=(LG_martti.otherPeers).length; i++) {
							if( LG_martti.otherPeers[i].name === hangingGet.peer_display_name){
								tempPeerId= i;
								if(!LG_martti.otherPeers[i].med_id){
									/*creating an array of remote peer objetcs*/
									var obj_mod = {
											peer : LG_martti.otherPeers[i].peer,
											name : hangingGet.peer_display_name,
											call_id : hangingGet.call_id,
											conf_id : hangingGet.conference_id,
											med_id : hangingGet.media_session_id,
											txn_id : hangingGet.transaction_id
										}
									LG_martti.otherPeers[i]= obj_mod;
								}
								break;
							}
						}
					}
					if(tempPeerId === -2){
						tempPeerId=LG_martti.otherPeersCount-1;
					}
                    handlePeerMessage(LG_martti.otherPeers[tempPeerId].peer, hangingGet);
                }
            } catch (e) {
				logWriter(e,"error");
            }
        }

		/*add meeting response */
		function signInCallback(response) {
			LG_martti.peerIdOthers=2;
			for(var i=0; i<response.MeetingInfo.Attendees.length; i++) {
				if(response.MeetingInfo.Attendees[i].User.DisplayName != LG_martti.userName) {
				updatePeers(true, LG_martti.peerIdOthers, response.MeetingInfo.Attendees[i].User.DisplayName);
				LG_martti.peerIdOthers += 1;
				}
				else {
				LG_martti.myId = 1;
				}
			}
			LG_martti.signedIn = true;
			LG_martti.callStatus = "started";
        }

		/*triggers the event to send "login" request via websockets*/
        function signIn(mail,roomNo) {
			var email = mail;
			var roomId = roomNo;
			$(document).trigger("loginEvent", [LG_martti.userName, email, roomId]);	
        }

		/*connect function is called on clicking the connect button*/
        function connect() {
			if(LG_martti.socketStatus === false){
				socketCreate();
				}
				/*setting turn and turn servers*/
				else{
					setConfigurationTurn();  
					/*getting the login parameters*/
					/*LG_martti.userName= loginObject.username;
					LG_martti.email=loginObject.emailId;
					LG_martti.roomid=loginObject.roomId;*/
					
					LG_martti.userName = document.getElementById("username").value;
					LG_martti.email = document.getElementById("email_id").value;
					LG_martti.roomid = document.getElementById("room_no").value;
					
					if (LG_martti.roomid) {
							signIn(LG_martti.email,LG_martti.roomid);
						}
			}
        }

		/*api called for calling a remote peer*/
        function outgoingCall(peer_id) {
            hcap.webrtc.outgoingCall({
                "localId": LG_martti.myId,
                "remoteId": peer_id,
                "onSuccess": function() {
                    LG_martti.callStatus = "started";
                    logWriter("outgoingCall","success");
                },
                "onFailure": function(f) {
                    LG_martti.callStatus = "ended";
                    logWriter("outgoingCall","failed");
                }
            });
        }

        function init() {
            init_key_listener();	
			/*listens to the event sent by hcap server*/
            document.addEventListener(
                "webrtc_message_received",
                function (param) {                                                    
                    sendMessage(param.remotePeerId, param.message);
                },
                false
            );
            
            document.addEventListener(
                "webrtc_event_received",
                function (param) {					
					if(param.eventString == "p2p_av_disconnected"){
						LG_martti.isConnected = false;
					} else if(param.eventString == "p2p_av_connected"){
						LG_martti.isConnected = true;
					}
                },
                false
            );
            
			hcap.property.setProperty({
				 "key" : "browser_https_security_level", 
				 "value" : "1", 
				 "onSuccess" : function() {
					 logWriter("setProperty","success");
				 }, 
				 "onFailure" : function(f) {
					  logWriter("setProperty","failed");
				 }
			});
			randomString();
        }
		
		function logWriter(info,value){
			var today =new Date();
			var logMessage="--"+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()+ "--"+info+"--"+value;
			console.log(logMessage);
		}
        
		function randomString() {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var string_length = 4;
            var randomstring = '';
            for (var i = 0; i < string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
            }
            //document.getElementById("local").value = "SHINTV-Pyeongtaek " + randomstring;
            document.getElementById("local").value = "SHINTVSeochoLab" + randomstring;
        }
		
        window.addEventListener('load', init);
