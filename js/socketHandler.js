
/*globals  io , console , users , peerConnection , $ , count:true, video_src,connection_obj, localStream */
/*global startLocalVideo , createpeerobject , setremoteanswer , seticecandidates , clearImmediate */
/*exported  SocketHandler,emitSdp,emitIce*/
/* jshint proto: true */
"use strict";

// Creation of Transaction ID

var lgEnviron = {
    capabilities : ["web", "html5content", "content", "chat"],
    hostPath : "https://meetme.carenection.com/",
    realm: "anonymous",
    userAgent: "Polycom_RPWS_Client/2.1.2.730-229386 (WebRTC; ECS_User) ",
    protocolVersion: "Collabutron v0.1",
    keyStr : "ice_cfg",
	valueStr : "stun:67.154.189.57:3478;turn:67.154.189.57:3478|meaturn|Polycom!23;",
	rid2:"rid_2",
	rid1:"rid_1",
	description1:"Adding a given modality to the modality list.",
	description2:"Dropping self from the meeting.",
	messageTypeOffer:"offer",
	messageTypeAnswer:"answer",
	messageTypeCandidate:"ice_candidate",
	messageTypeAlive:"keep_alive",
	messageTypeJoinConference:"join_conference_request"
}


var L, iter;
var nullCandidateFlag = true;
for (L = [], iter = 0; 256 > iter; iter++) {
    L[iter] = (16 > iter ? "0" : "") + iter.toString(16);
}

function get_transaction_id() {
    var a = 4294967295 * Math.random() | 0,
        b = 4294967295 * Math.random() | 0,
        c = 4294967295 * Math.random() | 0,
        d = 4294967295 * Math.random() | 0;
    return L[255 & a] + L[a >> 8 & 255] + L[a >> 16 & 255] + L[a >> 24 & 255] + "-" + L[255 & b] + L[b >> 8 & 255] + "-" + L[b >> 16 & 15 | 64] + L[b >> 24 & 255] + "-" + L[63 & c | 128] + L[c >> 8 & 255] + "-" + L[c >> 16 & 255] + L[c >> 24 & 255] + L[255 & d] + L[d >> 8 & 255] + L[d >> 16 & 255] + L[d >> 24 & 255];
}



//handling websocket and onclick events
var SocketHandler = function (signalingServerURL) {
    if (!this.socket) {
        this.socket = io(signalingServerURL);
        this.socket.on("connect", socketConnected);
        this.socket.on("disconnect", socketDisconnected);
        this.socket.on("SocketTestPong", socketPongEvent);
		$(document).on("socketClose", socketClose.bind(this, this.socket));
        var socket = this.socket;
        $(document).on("loginEvent", function (event, username, email, room) {
            var obj = {
                username: username,
                email: email,
				room: room
            };
            LG_martti.userInfo.push(obj);
            login(socket);
        });
        this.socket.on("Welcome", welcome.bind(this, this.socket));
        this.socket.on("AddMeeting", addMeeting.bind(this, this.socket));
        this.socket.on("UpdateAttendee", disconnectUpdateAttendee.bind(this, this.socket));
        this.socket.on("UserEnteredRoom", UserEnteredRoom.bind(this, this.socket));
        this.socket.on("PrefetchMeetingResponse", join.bind(this, this.socket));
        $(document).on("connectEvent", function () {
            join(socket);
        });
        $(document).on("sdpEvent", function (event, message, peer_id) {
            if (message) {
				emitSdp(socket, message, peer_id);
            }
        });
		this.socket.on("WebRTCResponse", hangingGetCallback.bind(this, this.socket));
        $(document).on("confirmDisconnectEvent", function () {
            disconnectFunction(socket);
        });
		$(document).on("keep_aliveEvent", function (event, resp) {
            keepAlive(socket,resp);
        });
        $(document).on("SendMsgEvent", function () {
            sendMsg(socket);
        });
        $(document).on("getMessageTextEventResponse", function (event, msg) {
            if (socket) {
                SendTextMessage(socket, msg);
            }
        });
        LG_martti.socketGlobal = this.socket;
   }    
};

var socketConnected = function () {
    startPing(this);
	LG_martti.socketStatus = true;
	connect();
};

var socketDisconnected = function () {
	LG_martti.socketStatus = false;
	pageReload();
    stopPing(this);
};

var startPing = function (socket) {
    if (!LG_martti.pingHandler) {
        LG_martti.pingHandler = setInterval(function() {
            socket.emit("SocketTestPing", { sentTime: new Date().toISOString() });
        }, LG_martti.pingInterval);
    }
};

var stopPing = function () {
    clearImmediate(LG_martti.pingHandler);
    LG_martti.pingHandler = null;
};

var socketPongEvent = function () {
    console.debug(arguments);
};

var socketClose = function (socket, response) {
    socket.close();
	
};

// sending login request
var login = function (socketObject) {
    var check_login_flag = "Normal_login"; 
    startLogin(socketObject, check_login_flag);
};

var startLogin = function (socket, check_login_flag) {
    if (socket && check_login_flag === "Normal_login") {
        socket.emit("Login", {
            Capabilities :lgEnviron.capabilities,
            HostPath : lgEnviron.hostPath,
            DisplayName: LG_martti.userName,
            EndpointKey: 'NULL',
            PublicEndpointKey: "null",
            Realm: lgEnviron.realm,
            UserAddress: LG_martti.email,
            rid: lgEnviron.rid2
        });

    }
    if (socket && check_login_flag === "Logout_login") {
        socket.emit("Login", {
            Capabilities :lgEnviron.capabilities,
            HostPath : lgEnviron.hostPath,
            EndpointKey: LG_martti.endPointKey,
            PublicEndpointKey: LG_martti.publicEndPointKey,
            rid: lgEnviron.rid2
        });
		socketCloseTrigger();	
    }
};

// receiving welcome response
var welcome = function (socket, response) {
    var room_val = LG_martti.userInfo[0].room;
    LG_martti.endPointKey = response.PrivateUserSessionInfo.EndpointKey;
    LG_martti.publicEndPointKey = response.User.PublicEndpointKey;
    enterLobby(socket, LG_martti.endPointKey, room_val);
};

// sending enterLobby request
var enterLobby = function (socket, endPointKey, room_val) {
    socket.emit("EnterLobby", {
        LobbyCode: room_val,
        EndpointKey: endPointKey
    });
};

// receiving addMeeting response
var addMeeting = function (socket, response) {
    LG_martti.numberOfUsers = response.MeetingInfo.Attendees.length;
    LG_martti.userIdGlobal = response.Attendee.User.UserId;
    var userName = response.Attendee.User.DisplayName;
    LG_martti.meetingRefGlobal = response.MeetingInfo.MeetingRef;
    var updateAttendeeInfo = response.Attendee;
    var lobbyCode = response.MeetingInfo.LobbyCode;
	signInCallback(response);
    socket.emit("ClientStatusUpdate", {
        "description": lgEnviron.description1,
        "userDetails": {
            "id": LG_martti.userIdGlobal,
            "userName": userName + "(Me)",
            "meetingId": LG_martti.meetingRefGlobal
        }
    });
    updateAttendee(socket, updateAttendeeInfo);
    prefetchMeeting(socket, lobbyCode);
};

//sending updateAttendee request
var updateAttendee = function (socket, updateAttendeeInfo) {
    updateAttendeeInfo.Capabilities.push("audiovideo");
    updateAttendeeInfo.Modalities.push("audiovideo");
    socket.emit("UpdateAttendee", updateAttendeeInfo);
};

//sending prefetchMeeting request
var prefetchMeeting = function (socket, lobbyCode) {
    socket.emit("PrefetchMeeting", { LobbyCode: lobbyCode });
};

//sending webrtc join_conference_request
var join = function (socketObject, pre_response) {
    joinContinue(socketObject);
};

//sending join conference request
var joinContinue = function (socket) {
    if (socket) {
        socket.emit("WebRTCRequest", {
            MeetingRef: LG_martti.meetingRefGlobal,
            Content: {
                authorized: false,
                display_name:LG_martti.userName,
                message_type: lgEnviron.messageTypeJoinConference,
                passback: LG_martti.email,
                protocol_version: lgEnviron.protocolVersion,
                transaction_id: get_transaction_id(),
                user_agent: lgEnviron.userAgent,
                vmr_id:LG_martti.roomid
            },
            UserId: LG_martti.userIdGlobal
        });
    }
};

//sending logout request
var logoutFunction = function (socket) {
    if (socket) {
        socket.emit("Logout", {
            Capabilities: lgEnviron.capabilities,
            DisplayName: LG_martti.userName,
            EndpointKey: LG_martti.endPointKey,
            HostPath: lgEnviron.hostPath,
            PublicEndpointKey: LG_martti.publicEndPointKey,
            Realm: lgEnviron.realm,
            UserAddress: LG_martti.email,
            UserId: LG_martti.userIdGlobal,
            UserName: LG_martti.userName,
            rid: lgEnviron.rid1
        });
        var check_login_flag = "Logout_login";
        startLogin(socket, check_login_flag);
    }
};


//sending webrtc sdp
var emitSdp = function(socket, localDescription, response_id) {
	for(var i=1;i<=LG_martti.otherPeers.length;i++){
		if(LG_martti.otherPeers[i].peer === response_id){
			response_id=i;
			break;
		}
	}
	var localDescription = JSON.parse(localDescription);
    if (localDescription.type === "offer") {
        socket.emit("WebRTCRequest", {
            MeetingRef: LG_martti.meetingRefGlobal,
            Content: {
                conference_id: LG_martti.otherPeers[response_id].conf_id,
                media_session_id: LG_martti.otherPeers[response_id].med_id,
                call_id: LG_martti.otherPeers[response_id].call_id,
                message_type: lgEnviron.messageTypeOffer,
                sdp: localDescription.sdp,
                transaction_id: LG_martti.otherPeers[response_id].txn_id
            },
            UserId: LG_martti.userIdGlobal
        });
    }
    if (localDescription.type === "answer") {
        socket.emit("WebRTCRequest", {
            MeetingRef: LG_martti.meetingRefGlobal,
            Content: {
                conference_id: LG_martti.otherPeers[response_id].conf_id,
                media_session_id: LG_martti.otherPeers[response_id].med_id,
                call_id: LG_martti.otherPeers[response_id].call_id,
                message_type: lgEnviron.messageTypeAnswer,
                sdp: localDescription.sdp,
                transaction_id: LG_martti.otherPeers[response_id].txn_id
            },
            UserId: LG_martti.userIdGlobal
        });	
    }
	if (localDescription.type === "candidate") {
        socket.emit("WebRTCRequest", {
            MeetingRef: LG_martti.meetingRefGlobal,
            Content: {
                conference_id: LG_martti.otherPeers[response_id].conf_id,
                media_session_id:LG_martti.otherPeers[response_id].med_id,
                call_id: LG_martti.otherPeers[response_id].call_id,
                message_type: lgEnviron.messageTypeCandidate,
                ice_candidate: localDescription,
                transaction_id: LG_martti.otherPeers[response_id].txn_id
            },
            UserId: LG_martti.userIdGlobal
        });
		/* explicitly sending null candidate */
        if (nullCandidateFlag) {
			nullCandidateFlag=false;
	
            setTimeout(function() {
                socket.emit("WebRTCRequest", {
                    MeetingRef: LG_martti.meetingRefGlobal,
                    Content: {
                        conference_id: LG_martti.otherPeers[response_id].conf_id,
                        media_session_id: LG_martti.otherPeers[response_id].med_id,
                        call_id: LG_martti.otherPeers[response_id].call_id,
                        message_type: lgEnviron.messageTypeCandidate,
                        ice_candidate: null,
                        transaction_id: LG_martti.otherPeers[response_id].txn_id
                    },
                    UserId: LG_martti.userIdGlobal
                });
            }, 5000);
		}
	};
}

// remote user entered room event
var UserEnteredRoom = function (socket, response) {
	var progma;
	var name;
	var flag;
    flag = 1;
    name = response.Attendee.User.DisplayName;
	var object = {
        peerId : LG_martti.peerIdOthers,
		name : name,
		flag : flag
    }
	handleServerNotification(object);
	LG_martti.peerIdOthers += 1;
}

/*this function is called when the remote peer hangs up the call*/
var disconnectUpdateAttendee = function (socket, response) {
	var progma;
	var name;
	var flag;
    if (response.ConnectionState === "disconnected") {
		for (var i=1; i<=(LG_martti.otherPeers).length; i++) {
            if( LG_martti.otherPeers[i].name === response.User.DisplayName) {
			    progma = i;
				name = response.User.DisplayName;
				flag = 0;
				break;
             }
			var object = {
			    peerId : progma,
				name : name,
				flag : flag
            }
            handleServerNotification(object);
		}
    }
    if (response.MuteStatus && response.ExternalParticipant.ExternalDisplayName === LG_martti.userName) {
        LG_martti.disconnectUpdateAttendee = response;
        LG_martti.disconnectUpdateAttendee.state = "disconnected";
		if (response.ConnectionState === "disconnected") {
			endCall();
			logoutFunction(socket);
		}
    }  
};

//disconnecting local user
var disconnectFunction = function (socket) {
    socket.emit("ClientStatusUpdate", JSON.stringify({
        "description": lgEnviron.description2,
        "userDetails": {
            "id": LG_martti.userIdGlobal,
            "userName": LG_martti.userName + "(Me)",
            "meetingId": LG_martti.meetingRefGlobal
        }
    }));
    socket.emit("UpdateAttendee", LG_martti.disconnectUpdateAttendee);
};

//reload page after logout
var pageReload = function() {
    console.log("reload");
    document.location.reload();
};

var keepAlive = function(socket,resp) {
   socket.emit("WebRTCRequest", {       
       MeetingRef: LG_martti.meetingRefGlobal,
       Content: {
           message_type: lgEnviron.messageTypeAlive,
           transaction_id: resp.transaction_id,
           call_id: resp.call_id
       },
       UserId: LG_martti.userIdGlobal
    });		
};

var socketCloseTrigger = function () {
    $(document).trigger("socketClose");
}
