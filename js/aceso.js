$(function () {

    var prev = document.referrer || "http://apollo.local/?s=webrtc-cb-cca";

    $('.inviteMenu ul li').removeClass('selected');
    $('.inviteMenu ul li[link=tab1]').addClass('selected');
    $('input#mobile').focus();

    // key handling
    $(document).keydown(function (e) {
        console.log('keydown received: ' + e.keyCode);
        e['Code'] = e['keyCode'];
        var key = getkeys(e.keyCode);
        var handled = true;

        switch (key) {
            case 'UP':
            case 'DOWN':
                app.hideCallAlert(); // hide call alert
                changeMenuFocus(key); // switch tab
                break;

            case 'LEFT':
                if($('#callAlert').is(':visible')) {    // switch call button focus
                    changeCallAlertFocus(key);
                }
                else {  // backspace
                    var text = $('input#mobile').val();
                    $('input#mobile').val(text.slice(0, -1));
                }
                break;

            case 'RIGHT':
                if($('#callAlert').is(':visible')) {    // switch call button focus
                    changeCallAlertFocus(key);
                }
                break;

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                $('input#mobile').focus();
                handled = false;
                break;

            case 'ENTER':
                var btn = $('.active');
                if(btn.length > 0) {
                    btn.click();
                }
                else if($('.back-button').hasClass('selected')) {
                    $('.back-button').click();
                }
                break;

            case 'HOME':
            case 'BACK':
            case 'EXIT':
                $('.back-button').click();
                break;

            default:
                handled = false;
                break;
        }

        console.log('keydown ' + e.keyCode + (handled ? ' handled' : ' not handled'));

        if (handled) {
            event.preventDefault();
            return false;
        }
    });

    // back to the previous page
    $('.back-button').click(function () {
        location.href = prev;
    });
});

function changeMenuFocus(direction) {
    var curr = $('.inviteMenu ul li.selected');
    if(curr.length < 1) {
        $('.inviteMenu ul li').first().addClass('selected');
        return;
    }
    var next = curr;
    $('.inviteMenu ul li').removeClass('selected');
    if (direction == 'UP' || direction == 'LEFT') {
        next = curr.prev();
        if (next.length <= 0) {
            next = curr.siblings().last();
        }
    }
    else if (direction == 'DOWN' || direction == 'RIGHT') {
        next = curr.next();
        if (next.length <= 0) {
            next = curr.siblings().first();
        }
    }

    next.addClass('selected');
    $(".inviteContent>div").hide();
    if (!next.hasClass('back-button')) {
        next.click();
    }
}

function changeCallAlertFocus(direction) {
    var curr = $('#callAlert button.active');
    if(curr.length < 1) {
        $('#callAlert button').first().addClass('active');
        return;
    }
    var next = curr;
    $('#callAlert button').removeClass('active');
    if (direction == 'UP' || direction == 'LEFT') {
        next = curr.prev();
        if (next.length <= 0) {
            next = curr.siblings().last();
        }
    }
    else if (direction == 'DOWN' || direction == 'RIGHT') {
        next = curr.next();
        if (next.length <= 0) {
            next = curr.siblings().first();
        }
    }

    next.addClass('active');
}

function focusButton(btn) {
    if(!btn) {
        btn = $('button:visible').first();
    }
    btn = $(btn);
    blurButton();
    btn.addClass('active');
}

function blurButton() {
    $('a,button').removeClass('active');
}

function getkeys(keycode) {

    // button commands
    var keys = Array();

    keys[8] = 'BACKSPACE';

    keys[13] = 'ENTER';

    keys[27] = 'VIDEO';    // ESC - video fullscreen toggle

    keys[33] = 'PGUP';
    keys[34] = 'PGDN';
    keys[35] = 'END';
    keys[36] = 'HOME';
    keys[37] = 'LEFT';
    keys[38] = 'UP';
    keys[39] = 'RIGHT';
    keys[40] = 'DOWN';


    //keys[46] = 'POWR';

    keys[48] = '0';
    keys[49] = '1';
    keys[50] = '2';
    keys[51] = '3';
    keys[52] = '4';
    keys[53] = '5';
    keys[54] = '6';
    keys[55] = '7';
    keys[56] = '8';
    keys[57] = '9';

    keys[67] = 'CHDN';     // C key maps to channel-
    keys[88] = 'CHUP';     // X key maps to channel+

    keys[107] = 'VOLU';     // + on numpad
    keys[109] = 'VOLD';     // - on numpad

    keys[120] = 'RWND';     // F9
    keys[121] = 'PLAY';     // F10
    keys[122] = 'FFWD';     // F11
    keys[123] = 'STOP';     // F12

    keys[216] = 'MENU';

    // ******************************************************
    // LG
    if (navigator.userAgent.indexOf(';LGE ;') > -1) {

        keys[hcap.key.Code.BACK] = 'BACK';
        keys[hcap.key.Code.EXIT] = 'EXIT';
        keys[hcap.key.Code.ENTER] = 'ENTER';
        keys[hcap.key.Code.PAUSE] = 'PAUSE';
        keys[hcap.key.Code.PAGE_UP] = 'PGUP';
        keys[hcap.key.Code.PAGE_DOWN] = 'PGDN';
        keys[hcap.key.Code.LEFT] = 'LEFT';
        keys[hcap.key.Code.UP] = 'UP';
        keys[hcap.key.Code.RIGHT] = 'RIGHT';
        keys[hcap.key.Code.DOWN] = 'DOWN';
        keys[hcap.key.Code.NUM_0] = '0';
        keys[hcap.key.Code.NUM_1] = '1';
        keys[hcap.key.Code.NUM_2] = '2';
        keys[hcap.key.Code.NUM_3] = '3';
        keys[hcap.key.Code.NUM_4] = '4';
        keys[hcap.key.Code.NUM_5] = '5';
        keys[hcap.key.Code.NUM_6] = '6';
        keys[hcap.key.Code.NUM_7] = '7';
        keys[hcap.key.Code.NUM_8] = '8';
        keys[hcap.key.Code.NUM_9] = '9';
        keys[hcap.key.Code.PORTAL] = 'MENU';   // Portal Key is mapped to MENU on pillow speaker
        keys[hcap.key.Code.CH_DOWN] = 'CHDN';
        keys[hcap.key.Code.CH_UP] = 'CHUP';
        keys[hcap.key.Code.REWIND] = 'REWIND';
        keys[hcap.key.Code.PLAY] = 'PLAY';
        keys[hcap.key.Code.GUIDE] = 'PLAY';    // Guide is mapped as Play/pause
        keys[hcap.key.Code.FAST_FORWARD] = 'FORWARD';
        keys[hcap.key.Code.STOP] = 'STOP';
        keys[hcap.key.Code.MUTE] = 'MUTE';
        keys[hcap.key.Code.VOL_DOWN] = 'VOLDN';
        keys[hcap.key.Code.VOL_UP] = 'VOLUP';

        keys[hcap.key.Code.RED] = 'RED';
        keys[hcap.key.Code.GREEN] = 'GREEN';
        keys[hcap.key.Code.YELLOW] = 'YELLOW';
        keys[hcap.key.Code.BLUE] = 'BLUE';
    }

    var key = keys[keycode];
    return key;
}