$(document).ready(function() {	
		app.init();
	});
	
	var pwd ='',
	    indexDBSupport = true;
	var app = {
		init : function(){
			
			window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;			
			
			 if (!window.indexedDB) {
				 indexDBSupport = false;				 
			 } else {
				 indexDBSupport = true;
			 }
			 
			app.autoFit();
			if(window.document.referrer.lastIndexOf('home.html') == -1 && indexDBSupport === true ) {  
				$("#loader").show();
				setTimeout("getUser('login')", 3000);
			}
			$('input').keypress(app.keyPress);
			$("#submit").on('click', app.login);
			$("#okBtn, #alertContainer").on('click', function() { $("#alertDiv").hide(); });
			$(window).resize(app.autoFit);
			
		},
		autoFit : function() {
			$("#bg").height(  window.innerHeight+"px" );
			$("#wrapper").height(  window.innerHeight+"px" );
			$("#loader").height(  window.innerHeight+"px" );
		},
		keyPress : function(e) {			
			var code = (e.keyCode ? e.keyCode : e.which);
		     if (code == "13") { 
		    	 e.preventDefault();
		    	 app.login();		    	 
		     }
		},
		login : function() {
			if($("#uname").val() == "") {
				 
				$("#password").removeClass('redBorder');
				$("#uname").addClass('redBorder');
				$("#errorMsg").html("Please enter your username");
				return false;
			}
			if($("#password").val() == "") {
				 
				$("#uname").removeClass('redBorder');
				$("#password").addClass('redBorder');
				$("#errorMsg").html("Please enter your password");
				return false;
			}
			$("#password").removeClass('redBorder');
			$("#uname").removeClass('redBorder');
			$("#errorMsg").html("&nbsp;");
			$("#loader").show();
			app.encode();	
		},
		encode : function() {
			var postData = {				    
				    "password":$("#password").val()
				}
			$.ajax({
				method  : 'POST',
				url     : baseURL+"/rest/CNX/encode",
				data    : JSON.stringify(postData), 
				headers : {'Content-Type': 'application/json'},
				async	: false,
				statusCode	: {
					200 : function(res) { 
						pwd = res.password;
						app.auth($("#uname").val(), pwd);
					},
					401 : function(xhr) { 
						$("#alertDiv").show()
						$("#alertMsg").html("Unauthorized user");
					},
					500 : function(xhr) { 
						$("#alertDiv").show()
						$("#alertMsg").html("Network connectivity issue");
					}
				}
				// Handling SSL certificate issue
				,error: function(e) {
				    $("#loader").hide();
					$("#alertDiv").show()
					$("#alertMsg").html("Something went wrong. Please refresh the page.");					
		        }
				
			});
		},
		auth : function(uname, pword) {
			
			var postData = {					    
				    "username":uname,
				    "password":pword
				}
			$.ajax({
				method  : 'POST',
				url     : baseURL+"/rest/CNX/user/auth",
				data    : JSON.stringify(postData), 
				headers : {'Content-Type': 'application/json'},
				statusCode	: {
					200 : function(res) {
						localStorage.setItem('username', uname);
						localStorage.setItem('login', "yes");
						if(localStorage.getItem('userExists') != "yes" ) { 
							addUser(uname, pword);
						} else {
							window.location = "home.html";
						}
						
					},
					401 : function(xhr) { 
						$("#loader").hide();
						$("#alertDiv").show()
						$("#alertMsg").html("Unauthorized user");
					},
					500 : function(xhr) { 
						$("#loader").hide();
						$("#alertDiv").show()
						$("#alertMsg").html("Network connectivity issue");
					}
				}
				, //  Handling  certificate issue
				 error: function(e) {
					 if(e.status != 200){
					    $("#loader").hide();
						$("#alertDiv").show()
						$("#alertMsg").html("Something went wrong. Please refresh the page.");		
					 }
			        }
				
			});
		}
		
	};